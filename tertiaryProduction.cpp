#include <cstdlib>
#include <cstddef>
#include <iostream>
#include <fstream>
#include <c++/4.8.2/complex>

#include "TApplication.h"


#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"

#include "TGraph.h"
#include "TCanvas.h"
#include "TF1.h"

#include "TRandom3.h"

#include "TString.h"
#include "TObjString.h"
#include "PythiaParticle.hh"

#include "TLorentzVector.h"

using namespace std;

enum particle {
  kProton = 0,
  kNeutron = 1,
  kPiPlus = 2,
  kPiMinus = 3
};

struct UserTools {
  TString configFileName;
  TString parentFileName;
  TString daughterFileName[4];
  TString chiFileName;
  Long64_t nMesonsFromDaughter[4];
  Long64_t nDaughters[4];
  Long64_t nDaughtersMomCut[4];
  Int_t pdgIds[4] = {2212, 2112, 211, -211};
  std::vector<Int_t> beamEnergies;
  TF1* chiBBbar[4];
  TF1* chiBBbarNorm[4];
  TString outputFileName;
};

void ReadConfigurationFile(UserTools& tools);
void ProcessParentFile(UserTools& tools);

int main(int argc, char** argv) {
  TApplication app("app", &argc, argv);

  UserTools tools;
  tools.configFileName = TString(app.Argv(1));
  ReadConfigurationFile(tools);

  gRandom->SetSeed(0);
  ProcessParentFile(tools);

  printf("****************DONE******************************************\n");
  app.Run(kTRUE);
  return 0;
}

void ReadConfigurationFile(UserTools& tools) {
  ifstream confFile(tools.configFileName.Data());
  if (!confFile.is_open()) {
    perror(tools.configFileName.Data());
    exit(1);
  }

  printf("Reading configuration file: %s \n", tools.configFileName.Data());

  //tools.beamEnergies.resize(0);
  TString Line;
  while (Line.ReadLine(confFile)) {
    if (Line.BeginsWith("#")) continue;
    TObjArray *l = Line.Tokenize(" ");

    if (Line.BeginsWith("ParentFileName= ")) {
      tools.parentFileName = ((TObjString*) (l->At(1)))->GetString();
      printf("Parent file name = %s \n", tools.parentFileName.Data());
      continue;
    }

    if (Line.BeginsWith("ProtonFileName= ")) {
      tools.daughterFileName[kProton] = ((TObjString*) (l->At(1)))->GetString();
      printf("Proton file name = %s \n", tools.daughterFileName[kProton].Data());
      continue;
    }


    if (Line.BeginsWith("NeutronFileName= ")) {
      tools.daughterFileName[kNeutron] = ((TObjString*) (l->At(1)))->GetString();
      printf("Neutron file name = %s \n", tools.daughterFileName[kNeutron].Data());
      continue;
    }


    if (Line.BeginsWith("PiPlusFileName= ")) {
      tools.daughterFileName[kPiPlus] = ((TObjString*) (l->At(1)))->GetString();
      printf("PiPlus file name = %s \n", tools.daughterFileName[kPiPlus].Data());
      continue;
    }


    if (Line.BeginsWith("PiMinusFileName= ")) {
      tools.daughterFileName[kPiMinus] = ((TObjString*) (l->At(1)))->GetString();
      printf("PiMinus file name = %s \n", tools.daughterFileName[kPiMinus].Data());
      continue;
    }

    if (Line.BeginsWith("EBEAMValues= ")) {
      TString dummy;
      Ssiz_t from = 13;
      while (Line.Tokenize(dummy, from, " ")) {
        tools.beamEnergies.push_back(dummy.Atoi());
      }
      printf("Available beam energies = ");
      for (UInt_t i = 0; i < tools.beamEnergies.size(); i++) {
        printf("%d ", tools.beamEnergies[i]);
      }
      printf("\n");
      continue;
    }

    if (Line.BeginsWith("ChiFileName= ")) {
      tools.chiFileName = ((TObjString*) (l->At(1)))->GetString();
      printf("File containing fits for chi_norm = %s \n", tools.chiFileName.Data());

      TFile chiFile(tools.chiFileName.Data(), "READ");
      TF1 *dummy1 = ((TGraph*) chiFile.Get("Chi_pp"))->GetFunction("f1");

      tools.chiBBbar[kProton] = new TF1("chiProton",
        "(x<300.)*(([p0]+[p1]*x+[p2]*pow(x,2)+[p3]*pow(x,3)+[p4]*pow(x,4)))+(x>=300.)*(([p5]+[p6]*x+[p7]*pow(x,2)))",
        120., 400.);
      tools.chiBBbar[kProton]->SetParameters(dummy1->GetParameters());
      
      tools.chiBBbar[kNeutron] = new TF1("chiNeutron",
        "(x<300.)*(([p0]+[p1]*x+[p2]*pow(x,2)+[p3]*pow(x,3)+[p4]*pow(x,4)))+(x>=300.)*(([p5]+[p6]*x+[p7]*pow(x,2)))",
        120., 400.);
      tools.chiBBbar[kNeutron]->SetParameters(dummy1->GetParameters());

      TF1 *dummy2 = new TF1(* ((TGraph*) chiFile.Get("Chi_piproton"))->GetFunction("f1"));
      tools.chiBBbar[kPiPlus] = new TF1("chiPiPlus",
        "(x<300.)*(([p0]+[p1]*x+[p2]*pow(x,2)+[p3]*pow(x,3)+[p4]*pow(x,4)))+(x>=300.)*(([p5]+[p6]*x+[p7]*pow(x,2)))",
        120., 400.);
      tools.chiBBbar[kPiPlus]->SetParameters(dummy2->GetParameters());
      tools.chiBBbar[kPiMinus] = new TF1("chiPiMinus",
        "(x<300.)*(([p0]+[p1]*x+[p2]*pow(x,2)+[p3]*pow(x,3)+[p4]*pow(x,4)))+(x>=300.)*(([p5]+[p6]*x+[p7]*pow(x,2)))",
        120., 400.);
      tools.chiBBbar[kPiMinus]->SetParameters(dummy2->GetParameters());

      TF1 *dummyNorm = new TF1(* ((TGraph*) chiFile.Get("ChiNorm_pp"))->GetFunction("f1"));
      
      
      tools.chiBBbarNorm[kProton] = new TF1("chiNormProton",
        "(x<300.)*(([p0]+[p1]*x+[p2]*pow(x,2)+[p3]*pow(x,3)+[p4]*pow(x,4)))+(x>=300.)*(([p5]+[p6]*x+[p7]*pow(x,2)))",
        120., 400.);
      tools.chiBBbarNorm[kProton]->SetParameters(dummyNorm->GetParameters());
      
      chiFile.Close();
      continue;
    }

    printf("chi(pp->bbbar) = %7.4e\n", TMath::Power(96.,1./3.)*tools.chiBBbar[kProton]->Eval(400.));
    TCanvas *cChiNorm = new TCanvas();
    cChiNorm->Divide(2,2);
    cChiNorm->cd(1);
    tools.chiBBbar[kProton]->Draw();
    
    cChiNorm->cd(2);
    tools.chiBBbar[kNeutron]->Draw();
    
    cChiNorm->cd(3);
    tools.chiBBbar[kPiPlus]->Draw();
    
    cChiNorm->cd(4);
    tools.chiBBbar[kPiMinus]->Draw();
    
    if (Line.BeginsWith("OutputFileName= ")) {
      tools.outputFileName = ((TObjString*) (l->At(1)))->GetString();
      printf("Output file name = %s \n", tools.outputFileName.Data());
      continue;
    }
  }
}

void ProcessParentFile(UserTools &tools) {
  std::vector< std::vector<TTree*> >
    bMesonsTrees(4, std::vector<TTree*>(tools.beamEnergies.size()));

  std::vector< std::vector<Int_t> >
    totalBMesons(4, std::vector<Int_t> (tools.beamEnergies.size()));

  std::vector< std::vector<PythiaParticle*> >
    pythiaParticles(4, std::vector<PythiaParticle*> (tools.beamEnergies.size()));

  TCanvas *cSacrifice = new TCanvas();
  TFile *df = new TFile("Test_File.root", "RECREATE");
  for (Int_t i = 0; i < 4; i++) {
    for (Int_t j = 0; j < tools.beamEnergies.size(); j++) {
      TFile dummyFile(Form("%s_%dGeV.root", tools.daughterFileName[i].Data(),
        tools.beamEnergies[j]), "READ");
      df->cd();
      bMesonsTrees[i][j] = ((TTree*) dummyFile.Get("Particle"))->CloneTree();
      totalBMesons[i][j] = bMesonsTrees[i][j]->GetEntries();
      printf("Number of B mesons from %d at %d GeV = %d\n",
        tools.pdgIds[i], tools.beamEnergies[j], totalBMesons[i][j]);
      pythiaParticles[i][j] = new PythiaParticle();
      bMesonsTrees[i][j]->SetBranchAddress("PythiaParticle", &pythiaParticles[i][j]);
    }
  }

  TFile outputFile(tools.outputFileName.Data(), "RECREATE");
  TTree *outputTree = new TTree("Particle", "Particle");

  PythiaParticle *outPart = new PythiaParticle();
  outputTree->Branch("PythiaParticle", outPart);

  TFile parentFile(tools.parentFileName.Data(), "READ");
  if (parentFile.IsZombie()) {
    printf("ERROR in parent File %s\n", parentFile.GetTitle());
  }
  printf("****************************************************************\n");
  printf("Opened parent file %s \n", parentFile.GetName());


  TTree* tree = (TTree*) parentFile.Get("NTuple");
  cout << "Tree pointer " << tree << endl;
  TNtuple* ntuple = (TNtuple*) parentFile.Get("NTuple/newPart");
  cout << "ntuple pointer " << ntuple << endl;

  Double_t x, y, z, Px, Py, Pz, t;
  Double_t PDGid, EventID, TrackID, ParentID;
  Double_t Weight, Bx, By, Bz, Ex, Ey, Ez, ProperTime, PathLength;
  Double_t PolX, PolY, PolZ, InitX, InitY, InitZ, InitT, InitKE;

  cout << "Ntuple definition... " << endl;

  ntuple->SetBranchAddress("x", &x);
  ntuple->SetBranchAddress("y", &y);
  ntuple->SetBranchAddress("z", &z);
  ntuple->SetBranchAddress("Px", &Px);
  ntuple->SetBranchAddress("Py", &Py);
  ntuple->SetBranchAddress("Pz", &Pz);
  ntuple->SetBranchAddress("t", &t);
  ntuple->SetBranchAddress("PDGid", &PDGid);
  ntuple->SetBranchAddress("EventID", &EventID);
  ntuple->SetBranchAddress("TrackID", &TrackID);
  ntuple->SetBranchAddress("ParentID", &ParentID);
  ntuple->SetBranchAddress("Weight", &Weight);

  printf("There are %d particles in the Ntuple\n", (int) ntuple->GetEntries());
  Double_t masses[4] = {0.938272, 0.939565, 0.139571, 0.139571};
  Double_t bMesonMass = 5.27932;
  //Double_t prodRates[4] = {0.25, 0.7,3.,3.};
  Double_t prodRates[4] = {0., 0., 0., 0.};
  Long64_t nEntries = ntuple->Draw("PDGid", "");
  Double_t testNParts = 0.0;
  //printf("Out of which only %llu with momentum in the range of SHiP\n", nEntries);
  for (Int_t i = 0; i < 4; i++) {
    tools.nDaughters[i] = ntuple->Draw("PDGid", Form("PDGid==%d", tools.pdgIds[i]));
    tools.nDaughtersMomCut[i] = ntuple->Draw("PDGid", Form("PDGid==%d && sqrt(Px*Px+Py*Py+Pz*Pz)/1000.>150.", tools.pdgIds[i]));
    prodRates[i] = 1.0 * tools.nDaughters[i] / (TMath::Power(10.0, 6.0));
    printf("Production rate for %d = %e\n", tools.pdgIds[i], prodRates[i]);
    printf("Fraction = %e\n", (1.0*tools.nDaughtersMomCut[i])/(1.0*tools.nDaughters[i]));
    testNParts += 1.0*tools.nDaughters[i];
  }
  printf("Total Useful daughters = %e\n", testNParts);
  TRandom3 *rndm = new TRandom3();
  Int_t nTrials = 1;
  for (Long64_t iEntry = 0; iEntry < nEntries; iEntry++) {
    ntuple->GetEntry(iEntry);
    if (iEntry % 1000 == 0)
      std::cout << "Porcessing event: " << iEntry << "\r" << std::flush;

    Int_t particleID = -1;
    if (PDGid == 2212)
      particleID = kProton;
    else if (PDGid == 2112)
      particleID = kNeutron;
    else if (PDGid == 211)
      particleID = kPiPlus;
    else if (PDGid == -211)
      particleID = kPiMinus;

    if (particleID == -1)
      continue;
    TLorentzVector mom;
    mom.SetXYZM(Px / 1000., Py / 1000., Pz / 1000., masses[particleID]);
    if (mom.Vect().Mag() < 150.)
      continue;
    Double_t energy = mom.Energy();
    TVector3 boostToCM = mom.BoostVector();
    TVector3 daughterDir = mom.Vect().Unit();
    
    Double_t energyBin = energy / 50.;
    Int_t idEnergy = round(energyBin) - 2;
    if (idEnergy < 0)
      idEnergy = 0;
    else if (idEnergy > tools.beamEnergies.size() - 1)
      idEnergy = tools.beamEnergies.size() - 1;
    energyBin = tools.beamEnergies[idEnergy];
    Double_t gammaBin = energyBin / masses[particleID];
    Double_t betaBin = TMath::Sqrt(gammaBin * gammaBin - 1.) / gammaBin;
    
    TVector3 momBin(0,0,betaBin*gammaBin*masses[particleID]);
    TVector3 daughterDirBin = momBin.Unit();
    
    TVector3 rotAxis = daughterDir.Cross(daughterDirBin).Unit();
    Double_t rotAngle = TMath::ACos(daughterDir.Dot(daughterDirBin));
    
    TLorentzVector momBinRotated;
    momBin.Rotate(-1*rotAngle,rotAxis);
//    momBin.Unit().Print();
//    mom.Vect().Unit().Print();
//    printf("\n");
    momBinRotated.SetVectM(momBin,masses[particleID]);
    TVector3 boostToCMRot = momBinRotated.BoostVector();
    
    for (Int_t iTrial = 0; iTrial < nTrials; iTrial++) {
      Double_t randNumb = rndm->Rndm();

      bMesonsTrees[particleID][idEnergy]->SetBranchAddress("PythiaParticle", &pythiaParticles[particleID][idEnergy]);
      Int_t flag = bMesonsTrees[particleID][idEnergy]->GetEntry(
        round(randNumb * totalBMesons[particleID][idEnergy]));

      TLorentzVector bMom;
      bMom.SetXYZM(pythiaParticles[particleID][idEnergy]->GetMomentum().X(),
        pythiaParticles[particleID][idEnergy]->GetMomentum().Y(),
        pythiaParticles[particleID][idEnergy]->GetMomentum().Z(),
        bMesonMass);
      bMom.Rotate(-1*rotAngle, rotAxis);
      
      TLorentzVector bMomFinal(bMom);
      bMomFinal.Boost(1. * boostToCMRot);
      bMomFinal.Boost(-1.*boostToCM);
      Double_t prodProb = tools.chiBBbar[particleID]->Eval(mom.Vect().Mag());
      outPart->SetMomentum(TVector3(bMomFinal.Vect()));
      //outPart->SetProbability(prodRates[particleID]*1E-6/nTrials);
      outPart->SetProbability(TMath::Power(63.,1./3.)*1.5*prodProb*1E-6/nTrials/
        ((1.0*tools.nDaughtersMomCut[particleID])/(1.0*tools.nDaughters[particleID])));
      outputTree->Fill();
    }
  }
  parentFile.Close();
  delete rndm;

  df->Close();
  delete df;

  outputFile.cd();
  outputTree->Print();
  outputFile.Write(0, TObject::kOverwrite);
  outputFile.Close();
}