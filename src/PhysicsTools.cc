#include "PhysicsTools.hh"
#include "math.h"
#include "TComplex.h"
#include "TF1.h"

namespace PhysicsTools {
  //Physical constants
  const Double_t hc = 197.326 * TMath::Power(10, -3); //GeV*fm
  const Double_t alpha = 1. / 137.;
  const Double_t hbar = 6.582E-25; //GeV*s
  const Double_t weakCouplingConstant = 1.0E-5;
  const Double_t wilsonCoeffDM2 = TMath::Power(4.9E-8, 2); // in GeV^-4
  const Double_t pi = 3.1415;
  const Double_t ec = 8.54E-2;
  const Double_t mg = 1. / 1.444; //Tesla to eV^2
  Double_t wilsonCoeffL = 0.;
  Double_t wilsonCoeffR = 2.8E-8;
  //Masses
  const Double_t mBquark = 4.18; //GeV
  const Double_t mBmeson = 5.279; //GeV
  const Double_t mKPlusMeson = 0.493677; //GeV
  const Double_t mK0Meson = 0.497611; //GeV
  const Double_t mKStarPlusMeson = 0.89166; //GeV
  const Double_t mKStar0Meson = 0.89581; //GeV
  Double_t mKmeson = 0.;
  const Double_t mTquark = 172.44; //GeV
  const Double_t mSquark = 0.096; //GeV
  const Double_t mWboson = 80.386; //GeV
  const Double_t mMuPlus = 0.1057; //GeV
  const Double_t mPiPlus = 0.13957; //GeV
  const Double_t mPi0 = 0.13497; // GeV
  const Double_t mVhiggs = 246.22; //GeV
  const Double_t xt2 = TMath::Power(mTquark / mWboson, 4); //fraction M_top/m_W squared
  //Quantities based on masses

  Double_t mBmKsum;
  Double_t mBmKdiff2;
  Double_t mBmesonThree = TMath::Power(mBmeson, 3);
  Double_t mBmesonMtwo = TMath::Power(mBmeson, -2);
  Double_t mBmesonMfour = TMath::Power(mBmeson, -4);
  Double_t mBmesonMsix = TMath::Power(mBmeson, -6);
  Double_t mBquarkTwo = TMath::Power(mBquark, 2); //mBquark^2

  void ComputeAditionalConstants() {
    mBmKsum = TMath::Power(mBmeson, 2) + TMath::Power(mKmeson, 2);
    mBmKdiff2 = TMath::Power(TMath::Power(mBmeson, 2) - TMath::Power(mKmeson, 2), 2);
  }


  //Decay widths
  const Double_t gammaBPlus = hbar / (1.638E-12); //GeV
  const Double_t gammaBZero = hbar / (1.520E-12); //GeV

  //NA62 magnet
  const Double_t dMagnet = 1.3; //m
  const Double_t zMagnet = 197.; //m
  const Double_t bMagnet = 0.6928; //T
  const Double_t beamHoleRadius = 0.1;

  //Dark scalar parameters to scan
  Double_t darkScalarMassMin = 0.;
  Double_t darkScalarMassMax = 0.;
  Int_t darkScalarMassNPoints = 0;
  Double_t darkScalarThetaMin = 0.;
  Double_t darkScalarThetaMax = 0.;
  Int_t darkScalarThetaNPoints = 0;
  Double_t darkScalarLambdaMin = 0.;
  Double_t darkScalarLambdaMax = 0.;
  Int_t darkScalarLambdaNPoints = 0;
  TString bMesonDecayMode;
  TString model;
  //Quantities and parameters for running the ToyMC
  TString parentMesonType;
  TString bMesonsParamsFileName;
  TString outputFileName;
  std::vector<Int_t> indexesUsefullB;
  std::vector<TString> flavoursBmesons;

  //Experiment parameters
  TString expetimentName;
  TString datatakingType;
  Double_t zMUV3;
  Double_t zLKR;
  Double_t zFVIn;
  Double_t zFVEnd;
  Double_t zTAX;
  Double_t x0;
  Double_t y0;
  Double_t z0;
  Double_t sigAcceptance;
  Double_t POT;
  Double_t aTarget = 63.5;
  Double_t zTarget;

  const Double_t zStraw[4] = {183.508, 194.066, 204.459, 218.885};
  const Double_t xStrawCenter[4] = {0.1012, 0.1144, 0.0924, 0.0528};
  const Double_t rStrawChambers = 0.0638;
  //Histograms
  TFile *outputFile;
  TCanvas *cDarkScalarDecay;
  TH1D *hDarkScalarDecayZvtx;
  TH2D *hDarkScalarToDiMuVertexPos;

  TCanvas *cProtonBeam;
  TH2D *hProtonBeamSpot;
  TH2D *hProtonBeamSpotMatched;

  TCanvas *cMuonPosAtMuv3;
  TH2D *hMuMinuxPosAtMUV3;
  TH2D *hMuPlusPosAtMUV3;

  TCanvas *cMuonNStrawChambers;
  TH1D *hMuMinusNStrawChambers;
  TH1D *hMuPlusNStrawChambers;

  TCanvas *cDarkScalarThetaXY;
  TH1D *hDarkScalarThetaX;
  TH1D *hDarkScalarThetaY;

  TH1D *hDarkScalarAcceptedMom;
  TH1D *hDarkScalarRejectedMom;
  //Signal Acceptance
  TCanvas *cSignalAcceptance;
  TH2D *hSigAccDSmassTheta;

  TCanvas *cSignalAcceptanceDSmassTheta2;
  TH2D *hSigAccDSmassTheta2;
  TH2D *hRelativeAcceptance;
  
  TCanvas *cBrBtoXmissE;

  TCanvas *cBMeson;
  TH1D *hBMesonMomentum;
  //Random number generator
  TRandom3 randGen;


  std::vector<TF1*> funcDecayWidthInclusive(0);
  std::vector<Double_t> gammaBtoSSInclusive(0);
  std::vector< std::vector<TF1*> > funcDecayWidthExclusive(0, std::vector<TF1*>(0)); //B->K and B->K*
  std::vector< std::vector<Double_t> > gammaBtoSSExclusive(0, std::vector<Double_t>(0)); // Same as above
  std::vector<Double_t> probabilityBtoKstar(0);
  std::vector<Double_t> probabilityBtoSSinclusiveFaser(0);
  std::vector<Double_t> dsMasses;
  std::vector<Double_t> thetas;
  Double_t normK;
  Double_t normKStar;
  //Follows Pospelov's paper (values and notations):  arXiv:hep-ph/0401195v4

  MathTools::MathTools() {
    m_darkScalarMass = 0.;
    m_lambda = 0.;
    m_theta = 0;
    m_gammaStoDiMu = 0.;
    m_gammaStoPiPi = 0.;
    m_parentMass = 0.;
    m_daughterMass = 0.;
  }

  void MathTools::ComputeDarkScalarLabMom(const Int_t iMass) {
    Double_t sEnergy = 0.5 * (TMath::Power(m_parentMass, 2)
      + TMath::Power(dsMasses[iMass], 2) - TMath::Power(m_daughterMass, 2)) / m_parentMass;
    Double_t sMomMag = TMath::Sqrt(TMath::Power(sEnergy, 2) - TMath::Power(dsMasses[iMass], 2));
    Double_t thetaS = TMath::ACos(1 - 2. * randGen.Rndm());
    Double_t phiS = TMath::TwoPi() * randGen.Rndm();

    m_DarkScalar4MomAccepted.SetVectM(TVector3(sMomMag * TMath::Sin(thetaS) * TMath::Cos(phiS),
      sMomMag * TMath::Sin(thetaS) * TMath::Sin(phiS),
      sMomMag * TMath::Cos(thetaS)), dsMasses[iMass]);

    m_DarkScalar4MomAccepted.Boost(m_bMeson4Mom.BoostVector());
  }

  //  void MathTools::ComputeDarkScalarLabMom(const Int_t iMass) {
  //    if (model.CompareTo("Singlet") == 0)
  //      DecayBtoSinglet(iMass);
  //    else
  //      DecayBtoDoublet(iMass);
  //  }

  void MathTools::DecayBtoSinglet(const Int_t iMass) {
    Double_t sEnergy = 0.5 * (TMath::Power(mBquark, 2)
      + TMath::Power(dsMasses[iMass], 2) - TMath::Power(mSquark, 2)) / mBquark;
    Double_t sMomMag = TMath::Sqrt(TMath::Power(sEnergy, 2) - TMath::Power(dsMasses[iMass], 2));
    Double_t thetaS = TMath::ACos(1 - 2. * randGen.Rndm());
    Double_t phiS = TMath::TwoPi() * randGen.Rndm();

    m_DarkScalar4MomAccepted.SetVectM(TVector3(sMomMag * TMath::Sin(thetaS) * TMath::Cos(phiS),
      sMomMag * TMath::Sin(thetaS) * TMath::Sin(phiS),
      sMomMag * TMath::Cos(thetaS)), dsMasses[iMass]);

    m_DarkScalar4MomAccepted.Boost(m_bMeson4Mom.BoostVector());
  }

  void MathTools::DecayBtoDoublet(const Int_t iMass) {
    Double_t invariantMassSS;
    if (bMesonDecayMode.CompareTo("Inclusive") == 0) {
      invariantMassSS =
        funcDecayWidthInclusive[iMass]->GetRandom(funcDecayWidthInclusive[iMass]->GetXmin(),
        funcDecayWidthInclusive[iMass]->GetXmax()); //* mBquarkTwo;
    } else {
      Int_t dummy = randGen.Binomial(1, probabilityBtoKstar[iMass]);
      invariantMassSS = funcDecayWidthExclusive[iMass][dummy]->GetRandom(funcDecayWidthExclusive[iMass][dummy]->GetXmin(),
        funcDecayWidthExclusive[iMass][dummy]->GetXmax());
      SetDaughterMass(dummy == 1 ? mKStarPlusMeson : mKPlusMeson);
    }

    Double_t diSenergy = 0.5 * (TMath::Power(m_parentMass, 2)
      + invariantMassSS - TMath::Power(GetDaughterMass(), 2)) / m_parentMass;
    Double_t diSmomMag = TMath::Sqrt(TMath::Power(diSenergy, 2) - invariantMassSS);
    Double_t thetadiS = TMath::ACos(1 - 2. * randGen.Rndm());
    Double_t phidiS = TMath::TwoPi() * randGen.Rndm();

    TLorentzVector ss4Mom;
    ss4Mom.SetVectM(TVector3(diSmomMag * TMath::Sin(thetadiS) * TMath::Cos(phidiS),
      diSmomMag * TMath::Sin(thetadiS) * TMath::Sin(phidiS),
      diSmomMag * TMath::Cos(thetadiS)), TMath::Sqrt(invariantMassSS));

    //Now, force the decay of the SS system with mass = TMath::Sqrt(invariantMassSS)
    //into to S particles of mass = darkScalarMass
    //thetaS,phiS are the angles between the momenta of the SS-system and one of 
    //the scalars
    Double_t thetaS = TMath::ACos(1. - 2. * randGen.Rndm());
    Double_t phiS = TMath::TwoPi() * randGen.Rndm();
    Double_t energyS = TMath::Sqrt(invariantMassSS) / 2;
    Double_t sMomMag = TMath::Sqrt(energyS * energyS - dsMasses[iMass] * dsMasses[iMass]);
    TLorentzVector s1FourMom(
      TVector3(sMomMag * TMath::Sin(thetaS) * TMath::Cos(phiS),
      sMomMag * TMath::Sin(thetaS) * TMath::Sin(phiS),
      sMomMag * TMath::Cos(thetaS)),
      energyS);
    TLorentzVector s2FourMom(-s1FourMom.Vect(), energyS);

    //Randomly choose one to keep
    m_DarkScalar4MomAccepted = randGen.Binomial(1, 0.5) == 0 ? s1FourMom : s2FourMom;
    m_DarkScalar4MomRejected = m_DarkScalar4MomAccepted == s1FourMom ? s2FourMom : s1FourMom;
    //Boost the one kept in the laboratory frame
    //this is a sequence boost:
    //first from SS frame to B frame
    //second from B frame to lab frame
    m_DarkScalar4MomAccepted.Boost(ss4Mom.BoostVector());
    m_DarkScalar4MomAccepted.Boost(m_bMeson4Mom.BoostVector());

  }

  void MathTools::InitializeBtoKSSdecayWidths(const std::vector<Double_t>& dsMasses, const Int_t &nPx) {
    Double_t squareInvariantMassMax = 0.;

    Double_t *xPoints = new Double_t[nPx];
    Double_t *wPoints = new Double_t[nPx];

    funcDecayWidthExclusive.resize(dsMasses.size());
    gammaBtoSSExclusive.resize(dsMasses.size());
    for (Int_t i = 0; i < darkScalarMassNPoints; i++) {
      this->SetDarkScalarMass(dsMasses[i]);
      if (bMesonDecayMode.CompareTo("Inclusive") == 0) {

        squareInvariantMassMax = TMath::Power(mBquark - mSquark, 2);
        funcDecayWidthInclusive.push_back(new TF1(Form("BKSSdiffDecayWidth_%d", i), this,
          &PhysicsTools::MathTools::DiffDecayWidthBSSInclusive,
          4 * dsMasses[i] * dsMasses[i], squareInvariantMassMax, 1,
          "PhysicsTools::MathTools", "DiffDecayWidthBSSInclusive"));
        funcDecayWidthInclusive[i]->SetParameter(0, dsMasses[i]);
        funcDecayWidthInclusive[i]->SetNpx(nPx);
        funcDecayWidthInclusive[i]->CalcGaussLegendreSamplingPoints(nPx, xPoints, wPoints, 1e-15);
        gammaBtoSSInclusive.push_back(funcDecayWidthInclusive[i]->IntegralFast(nPx, xPoints, wPoints, 4 * TMath::Power(dsMasses[i], 2),
          squareInvariantMassMax, 0, 1e-12));
        probabilityBtoSSinclusiveFaser.push_back(FaserDecayWidthBSSInclusive(dsMasses[i]) / gammaBPlus);
        //printf("Br(B->XSS) = %e\n", gammaBtoSSInclusive[i]/gammaBPlus);

      } else {

        squareInvariantMassMax = (TMath::Power(mBmeson - mKPlusMeson, 2));
        funcDecayWidthExclusive[i].push_back(new TF1(Form("BKSSdiffDecayWidth_%d", i), this,
          &PhysicsTools::MathTools::DiffDecayWidthBKSS,
          4 * TMath::Power(dsMasses[i]/*/mBmeson*/, 2), squareInvariantMassMax/*/Power(mBmeson, 2)*/, 1,
          "PhysicsTools::MathTools", "DiffDecayWidthBKSS"));
        funcDecayWidthExclusive[i][0]->SetParameter(0, dsMasses[i]/*/mBmeson*/);
        funcDecayWidthExclusive[i][0]->SetNpx(nPx);
        funcDecayWidthExclusive[i][0]->CalcGaussLegendreSamplingPoints(nPx, xPoints, wPoints, 1e-15);
        gammaBtoSSExclusive[i].push_back(funcDecayWidthExclusive[i][0]->IntegralFast(nPx, xPoints, wPoints, 4 * TMath::Power(dsMasses[i]/*/mBmeson*/, 2),
          squareInvariantMassMax/*/Power(mBmeson, 2)*/, 0, 1e-12));

        squareInvariantMassMax = (TMath::Power(mBmeson - mKStarPlusMeson, 2));
        funcDecayWidthExclusive[i].push_back(new TF1(Form("BKstarSSdiffDecayWidth_%d", i), this,
          &PhysicsTools::MathTools::DiffDecayWidthBKstarSS,
          4 * TMath::Power(dsMasses[i]/*/mBmeson*/, 2), squareInvariantMassMax/*/TMath::Power(mBmeson, 2)*/, 1,
          "PhysicsTools::MathTools", "DiffDecayWidthBKStarSS"));
        funcDecayWidthExclusive[i][1]->SetParameter(0, dsMasses[i]/*/mBmeson*/);
        funcDecayWidthExclusive[i][1]->SetNpx(nPx);
        funcDecayWidthExclusive[i][1]->CalcGaussLegendreSamplingPoints(nPx, xPoints, wPoints, 1e-15);
        gammaBtoSSExclusive[i].push_back(funcDecayWidthExclusive[i][1]->IntegralFast(nPx, xPoints, wPoints, 4 * TMath::Power(dsMasses[i]/*/mBmeson*/, 2),
          squareInvariantMassMax/*/Power(mBmeson, 2)*/, 0, 1e-12));

        probabilityBtoKstar.push_back(gammaBtoSSExclusive[i][1] / gammaBPlus);
      }

      std::cout << "Completed: " << 100 * i / darkScalarMassNPoints << "%..." << "\r" << std::flush;
    }
  }

  void MathTools::ComputeNormBtoK(Double_t& normKstar, Double_t& normK, const Int_t &nPx) {
    if (bMesonDecayMode.CompareTo("Exclusive") == 0) {
      Double_t *xPoints = new Double_t[nPx];
      Double_t *wPoints = new Double_t[nPx];
      Double_t squareInvariantMassMax = TMath::Power(mBmeson - mKStarPlusMeson, 2);
      TF1 *fNorm = new TF1(Form("BKstarSSdiffDecayWidth_%s", "norm1"), this,
        &PhysicsTools::MathTools::DiffDecayWidthBKstarSS,
        0., squareInvariantMassMax/*/TMath::Power(mBmeson, 2)*/, 1,
        "PhysicsTools::MathTools", "DiffDecayWidthBKStarSS");
      fNorm->SetParameter(0, 0.);
      fNorm->SetNpx(nPx);
      fNorm->CalcGaussLegendreSamplingPoints(nPx, xPoints, wPoints, 1e-15);
      normKstar = fNorm->IntegralFast(nPx, xPoints, wPoints, 0., squareInvariantMassMax, 0, 1e-12);

      squareInvariantMassMax = TMath::Power(mBmeson - mKPlusMeson, 2);
      fNorm = new TF1(Form("BKstarSSdiffDecayWidth_%s", "norm2"), this,
        &PhysicsTools::MathTools::DiffDecayWidthBKSS,
        0., squareInvariantMassMax/*/TMath::Power(mBmeson, 2)*/, 1,
        "PhysicsTools::MathTools", "DiffDecayWidthBKStarSS");
      fNorm->SetParameter(0, 0.);
      fNorm->SetNpx(nPx);
      fNorm->CalcGaussLegendreSamplingPoints(nPx, xPoints, wPoints, 1e-15);
      normK = fNorm->IntegralFast(nPx, xPoints, wPoints, 0., squareInvariantMassMax, 0, 1e-12);

      delete [] xPoints;
      delete [] wPoints;
      delete fNorm;
    }
  }

  Double_t MathTools::FaserDecayWidthBSSInclusive(Double_t x) {
    Double_t ms = x;
    return
    wilsonCoeffDM2 * TMath::Power(m_lambda, 2) * TMath::Power(mBquark, 5) *
      MagicFunc(ms / mBquark) / (256. * TMath::Power(TMath::Pi(), 3));
  }

  Double_t MathTools::DiffDecayWidthBSSInclusive(Double_t* x, Double_t * pars) {
    Double_t sb = x[0];
    Double_t ms = pars[0];

    /*return TMath::Power(mBquark, 5) * (TMath::Power(wilsonCoeffR, 2) + TMath::Power(wilsonCoeffL, 2)) /
      (TMath::Power(2, 9) * TMath::Power(TMath::Pi(), 3)) *
      TMath::Sqrt(1 - 4. * TMath::Power(ms, 2) / sb) *
      TMath::Sqrt(LambdaPhaseSpace(1., TMath::Power(mSquark / mBquark, 2), sb))*
      (1 + TMath::Power(mSquark / mBquark, 2) - sb - 4 * mSquark / mBquark * wilsonCoeffL * wilsonCoeffR /
      (TMath::Power(wilsonCoeffR, 2) + TMath::Power(wilsonCoeffL, 2)));
  
     */

    return (m_lambda * m_lambda * wilsonCoeffDM2 / (256. * TMath::Power(TMath::Pi(), 3) * mBquark))*
      TMath::Sqrt(1 - 4. * TMath::Power(ms, 2) / sb) * TMath::Sqrt(LambdaPhaseSpace(pow(mBquark, 2), pow(mSquark, 2), sb))*
      (TMath::Power(mBquark - mSquark, 2) - sb);
  }

  Double_t MathTools::LambdaPhaseSpace(const Double_t a, const Double_t b, const Double_t c) {
    return TMath::Power(a, 2) + TMath::Power(b, 2) + TMath::Power(c, 2) - 2. * (a * b + b * c + a * c);
  }

  Double_t MathTools::MagicFunc(Double_t x) {
    return 1. / 3. * TMath::Sqrt(1 - 4 * TMath::Power(x, 2))*(1. + 5. * x * x - 6 * TMath::Power(x, 6))
      - 4 * x * x * (1. - 2. * TMath::Power(x, 2) + 2. * TMath::Power(x, 4))
      * TMath::Log(1 / (2. * x) * (1 + TMath::Sqrt(1 - 4 * TMath::Power(x, 2))));
  }

  Double_t MathTools::DiffDecayWidthBKstarSS(Double_t* x, Double_t * pars) {
    Double_t invariantMassSS = x[0];
    Double_t ms = pars[0];
    return xt2 * wilsonCoeffDM2 * TMath::Power(m_lambda, 2) * BkFormFactor2(invariantMassSS) *
      PhaseSpaceBKstarSS(invariantMassSS, ms) * mBquarkTwo * TMath::Power(TMath::Power(mBmeson, 2) - TMath::Power(mKStarPlusMeson, 2), 2) /
      (512 * TMath::Power(pi, 3) * mBmesonThree * TMath::Power(mBquark - mSquark, 2));
    //    return TMath::PiOver2() * TMath::Power(mBmeson, 5) * 3 *
    //      TMath::Power(1.364/(1 - invariantMassSS / 5.28 / TMath::Power(mBmeson, 2)) + 
    //      -0.990/(1 - invariantMassSS/ 36.78 / TMath::Power(mBmeson, 2)), 2)*
    //      TMath::Power(wilsonCoeffL - wilsonCoeffR, 2) * 
    //      TMath::Sqrt(1. - 4. * TMath::Power(ms,2)/invariantMassSS) * 
    //      TMath::Power(LambdaPhaseSpace(1, TMath::Power(mKStarPlusMeson/mBmeson,2), invariantMassSS), 3./2.)/
    //      (TMath::Power(2, 12) * TMath::Power(TMath::Pi(),3));
  };

  Double_t MathTools::PhaseSpaceBKstarSS(Double_t invariantMassSS, Double_t ms) {
    Double_t phaseSpace2 = TMath::Sqrt(TMath::Power(invariantMassSS, 2) -
      2 * invariantMassSS * (mBmeson * mBmeson + TMath::Power(mKStarPlusMeson, 2)) +
      TMath::Power(mBmeson * mBmeson - TMath::Power(mKStarPlusMeson, 2), 2)) *
      TMath::Sqrt(1 - 4 * TMath::Power(ms, 2) / invariantMassSS);

    return phaseSpace2 > 0. ? phaseSpace2 : 0.;
  };

  Double_t MathTools::DiffDecayWidthBKSS(Double_t* x, Double_t * pars) {
    Double_t invariantMassSS = x[0];
    Double_t ms = pars[0];
    return xt2 * wilsonCoeffDM2 * TMath::Power(m_lambda, 2) * BkFormFactor2(invariantMassSS) *
      PhaseSpaceBKSS(invariantMassSS, ms) * mBquarkTwo * TMath::Power(TMath::Power(mBmeson, 2) - TMath::Power(mKPlusMeson, 2), 2) /
      (512 * TMath::Power(pi, 3) * mBmesonThree * TMath::Power(mBquark - mSquark, 2));

    //    return TMath::Power(mBmeson, 5) * TMath::Power(0.330 / (1 - invariantMassSS / 37.46 / TMath::Power(mBmeson, 2)), 2) *
    //      TMath::Power(1. - TMath::Power(mKPlusMeson / mBmeson, 2), 2) *
    //      TMath::Power(wilsonCoeffL + wilsonCoeffR, 2) * TMath::Sqrt(1. - 4 * TMath::Power(ms, 2) / invariantMassSS) *
    //      TMath::Sqrt(LambdaPhaseSpace(1, TMath::Power(mKPlusMeson / mBmeson, 2), invariantMassSS)) /
    //      (TMath::Power(2, 11) * TMath::Power(TMath::Pi(), 3));
  };

  Double_t MathTools::PhaseSpaceBKSS(Double_t invariantMassSS, Double_t ms) {
    Double_t phaseSpace2 = TMath::Sqrt(TMath::Power(invariantMassSS, 2) -
      2 * invariantMassSS * (mBmeson * mBmeson + TMath::Power(mKPlusMeson, 2)) +
      TMath::Power(mBmeson * mBmeson - TMath::Power(mKPlusMeson, 2), 2)) *
      TMath::Sqrt(1 - 4 * TMath::Power(ms, 2) / invariantMassSS);

    return phaseSpace2 > 0. ? phaseSpace2 : 0.;
  };

  Double_t MathTools::BkFormFactor2(Double_t invariantMassSS) {

    return
    TMath::Power(0.3 * exp(0.63 * invariantMassSS * mBmesonMtwo
      - 0.095 * TMath::Power(invariantMassSS, 2) * mBmesonMfour
      + 0.591 * TMath::Power(invariantMassSS, 3) * mBmesonMsix), 2);
  }

  TVector3 MathTools::ComputeStoDiMuVertex(const TVector3& sStartPos, const TLorentzVector& sStart4Mom, const Double_t lambda) {
    //First distribute uniformly along zAxis in Fiducial Volume
    Double_t seed = randGen.Rndm();
    Double_t a = TMath::Exp(-(zFVIn - sStartPos.Z()) / lambda);
    Double_t b = TMath::Exp(-(zFVEnd - sStartPos.Z()) / lambda);

    Double_t zVertex = -lambda * TMath::Log(a - seed * (a - b)) + sStartPos.Z();
    Double_t xVertex = sStartPos.X() + (zVertex - sStartPos.Z()) * sStart4Mom.X() / sStart4Mom.Z();
    Double_t yVertex = sStartPos.Y() + (zVertex - sStartPos.Z()) * sStart4Mom.Y() / sStart4Mom.Z();

    return TVector3(xVertex, yVertex, zVertex);
  }

  Int_t MathTools::IsInStraw(const TVector3& startPos, TLorentzVector& momentum, const Double_t charge) {
    Int_t nStrawChambers = 0;
    TVector3 initPos(startPos);
    TLorentzVector initMom(momentum);
    for (Int_t i = 0; i < 4; i++) {
      TVector2 chamberCenter(xStrawCenter[i], 0.);
      TVector3 endPos = Propagate(initPos, initMom, charge, zStraw[i]);
      Double_t dist = ((TVector2) (endPos.XYvector() - chamberCenter)).Mod();
      initPos = endPos;
      if (dist < rStrawChambers || dist > 1.)
        continue;
      nStrawChambers++;

    }
    return nStrawChambers;
  }

  TVector3 MathTools::Propagate(const TVector3& position, TLorentzVector& momentum, const Double_t charge, const Double_t zEnd) {
    TVector3 positionFinal(0., 0., 0.);

    if (zEnd <= zMagnet) {
      positionFinal.SetX(position.X() + (zEnd - position.Z()) * momentum.X() / momentum.Z());
      positionFinal.SetY(position.Y() + (zEnd - position.Z()) * momentum.Y() / momentum.Z());
      positionFinal.SetZ(zEnd);
      return positionFinal;
    }

    //Propagate at front of magnet
    positionFinal.SetXYZ(position.X() + (zMagnet - dMagnet / 2 - position.Z()) * momentum.X() / momentum.Z(),
      position.Y() + (zMagnet - dMagnet / 2 - position.Z()) * momentum.Y() / momentum.Z(),
      zMagnet - dMagnet / 2.);

    Double_t ptKick = -charge * ec * bMagnet * mg * dMagnet / hc; //GeV
    Double_t pXpZ2 = TMath::Power(momentum.X(), 2) + TMath::Power(momentum.Z(), 2);
    Double_t momXfinal = momentum.X() + ptKick;
    Double_t momZfinal = TMath::Sqrt(pXpZ2 - TMath::Power(momXfinal, 2));
    Double_t dx = hc * (momZfinal - momentum.Z()) / (charge * ec * bMagnet * mg); //m
    Double_t dy = TMath::Sqrt((TMath::Power(dMagnet, 2) + TMath::Power(dx, 2))) * momentum.Y() / TMath::Sqrt(pXpZ2); //m
    momentum.SetX(momXfinal);
    momentum.SetZ(momZfinal);

    positionFinal.SetXYZ(position.X() + dx + (zEnd - zMagnet + dMagnet / 2.) * momentum.X() / momentum.Z(),
      position.Y() + dy + (zEnd - zMagnet + dMagnet / 2.) * momentum.Y() / momentum.Z(),
      zEnd);

    return positionFinal;
  }

  void MathTools::ComputeDecayWidths() {
    if (m_darkScalarMass < 2.0 * mMuPlus) {
      printf("Error: dark scalar mass too low \n");
      exit(0);
    } else {
        m_gammaStoDiMu = TMath::Power(mMuPlus, 2) * m_darkScalarMass *
        TMath::Power(TMath::Sqrt(1 - 4 * TMath::Power(mMuPlus / m_darkScalarMass, 2)), 3) * TMath::Power(m_theta, 2) /
        (8 * TMath::Pi() * TMath::Power(mVhiggs, 2));
        
      if (m_darkScalarMass >= 2.0 * mPiPlus) {
        m_gammaStoPiPi = m_gammaStoDiMu * 0.75 * TMath::Sqrt(1 - 4 * TMath::Power(mPiPlus / m_darkScalarMass, 2)) /
          (TMath::Power(1 - 4 * TMath::Power(mMuPlus / m_darkScalarMass, 2), 3 / 2) * TMath::Power(mMuPlus*m_darkScalarMass, 2)) *
          StoPiPiFromFactors(TMath::Power(m_darkScalarMass, 2));
        //        m_gammaStoPiPi = 1.5*TMath::Power(m_darkScalarMass, 3)*TMath::Power(m_theta,2)*TMath::Sqrt(1 - 4*TMath::Power(mPiPlus/m_darkScalarMass,2))*
        //          TMath::Power(2/9 + 11/9*TMath::Power(mPiPlus/m_darkScalarMass,2), 2)/(16*TMath::Pi()*TMath::Power(mVhiggs,2));
        //        printf("mu = %e, pi = %e \n", m_gammaStoDiMu, m_gammaStoPiPi);
      } else m_gammaStoPiPi = 0.;
    }
  }

  Double_t MathTools::StoPiPiFromFactors(const Double_t& s) {
    Double_t sigma = TMath::Sqrt(1 - 4 * TMath::Power(mPiPlus / m_darkScalarMass, 2));
    TComplex phi(
      (2 * s - TMath::Power(mPiPlus, 2) / (32. * TMath::Power(TMath::Pi()* 0.093, 2)))*(sigma *
      TMath::Log((1 - sigma) / (1 + sigma)) + 2.) + s / (192 * TMath::Power(0.093 * pi, 2)),
      (2 * s - TMath::Power(mPiPlus, 2) / (32. * TMath::Power(TMath::Pi()* 0.093, 2))) * TMath::Pi() * sigma
      );
    TComplex thetaS = TComplex(s + 2 * TMath::Power(mPiPlus, 2), 0)*(TComplex(1., 0) + phi) + TComplex(2.7 * TMath::Power(s, 2), 0);
    TComplex gammaS = TComplex(TMath::Power(mPiPlus, 2), 0) *(phi + TComplex(1. + 2.6 * s, 0));
    TComplex delatS = TComplex(0.009 * s, 0)*(phi + TComplex(1. + 3.3 * s, 0));

    TComplex formFactor = TComplex(2. / 9., 0) * thetaS + TComplex(7 / 9, 0) * (gammaS + delatS);
    return formFactor.Rho();
  }
}
