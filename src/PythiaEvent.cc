#include "PythiaEvent.hh"

ClassImp(PythiaEvent)

PythiaEvent::PythiaEvent(): m_NParticles(0), m_particles(nullptr) {
}

PythiaEvent::PythiaEvent(Int_t nMaxParticles): m_NParticles(0) {
  m_particles = new TClonesArray("PythiaParticle", nMaxParticles);
}

PythiaEvent::~PythiaEvent() {
  m_NParticles = 0;
  if (m_particles){
    delete m_particles;
    m_particles = 0;
  }
}

void PythiaEvent::Clear(Option_t* option) {
  m_NParticles = 0;
  m_particles->Clear(option);
}

PythiaParticle* PythiaEvent::AddParticle() {
  return static_cast<PythiaParticle *> 
    ((m_particles->ConstructedAt(m_NParticles++,"C")));
}

