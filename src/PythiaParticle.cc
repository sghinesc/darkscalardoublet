#include "PythiaParticle.hh"

ClassImp(PythiaParticle)

PythiaParticle::PythiaParticle() : m_index(0), m_momentum(-999.,-999.,-999.), 
 m_probability(1), m_eventID(-1){
}


PythiaParticle::~PythiaParticle() {
}

void PythiaParticle::SetMomentum(const TVector3& p) {
  m_momentum.SetX(p.X());
  m_momentum.SetY(p.Y());
  m_momentum.SetZ(p.Z());
}

