#include <iostream>
#include <vector>

#include "TFile.h"

#include "TCanvas.h"
#include "TH2.h"
#include "TGraph.h"

void BuildContour(TH2D *h1, TH2D *h2, TH2D *hSum);

void HistoManipulation() {
  TFile filePrimary("../output/Primaries_BeamDump_1E16.root", "READ");
  TFile fileSecondary("../output/Secondaries_BeamDump_1E16.root", "READ");

  TH2D *hPrimaryYield = (TH2D*) filePrimary.Get("hSignalAcceptanceDSmassTheta2");
  TH2D *hSecondaryYield = (TH2D*) fileSecondary.Get("hSignalAcceptanceDSmassTheta2");
  TCanvas *cTest = new TCanvas();
  cTest->cd();
  hSecondaryYield->DrawCopy();
  TH2D *hSum = (TH2D*) hSecondaryYield->DrawClone();
  TH2D *hRatio = (TH2D*) hSecondaryYield->DrawClone();
  cTest->Update();
  
  Int_t nBinsX = hPrimaryYield->GetNbinsX();
  Int_t nBinsY = hPrimaryYield->GetNbinsY();
  printf("nBinsX = %i; nBinsY = %i\n", nBinsX, nBinsY);
  for (Int_t i = 0; i <= nBinsX; i++) {
    for (Int_t j = 0; j <= nBinsY; j++) {
      Double_t wp = hPrimaryYield->GetBinContent(i, j);
      Double_t ws = hSecondaryYield->GetBinContent(i, j);
      hSum->SetBinContent(i, j, wp + ws);
      hRatio->SetBinContent(i,j,wp<0.0001 ? 0. :  ws/(ws+wp));
    }
  }

  TCanvas *cSum = new TCanvas("cSum", "cSum");
  cSum->cd();
  hSum->DrawCopy("COLZ");
  cSum->Update();
  BuildContour(hPrimaryYield,hSecondaryYield, hSum);
  
  TCanvas *cRatio = new TCanvas("cRatio", "cRatio");
  cRatio->cd();
  hRatio->DrawCopy("COLZ");
  
  TCanvas *cAcceptance = new TCanvas("cAaceptance", "cAcceptance");
  cAcceptance->Divide(2,1);
  TH2D *hBmesonYieldPrim = (TH2D*) filePrimary.Get("hRelativeAcceptance");
  TH2D *hBmesonYieldSec = (TH2D*) fileSecondary.Get("hRelativeAcceptance");
  
  TH2D *hAcceptancePrimary = (TH2D*) hBmesonYieldPrim->DrawClone();
  TH2D *hAcceptanceSeconday = (TH2D*) hBmesonYieldSec->DrawClone();
  hPrimaryYield->Scale(1./(1.E16));
  hSecondaryYield->Scale(1./(1.E16));
  
  hAcceptancePrimary->Divide(hPrimaryYield,hBmesonYieldPrim);
  hAcceptanceSeconday->Divide(hSecondaryYield,hBmesonYieldSec);
  cAcceptance->cd(1);
  hAcceptancePrimary->DrawCopy("COLZ");
  cAcceptance->cd(2);
  hAcceptanceSeconday->DrawCopy("COLZ");
}

void BuildContour(TH2D *h1, TH2D *h2, TH2D *hSum) {
  TCanvas *cContours = new TCanvas("cContours", "cContours");
  TCanvas *cDummy = new TCanvas("cDummy", "cDummy");
  Double_t contourLevel[2] = {2.3, 2.3};
  std::vector<TGraph*> conts(0);

  TH2D *hFinal = (TH2D*) h1->DrawClone();
  for (Int_t i = 1; i <= hFinal->GetNbinsX(); i++) {
    for (Int_t j = 1; j <= hFinal->GetNbinsY(); j++) {
      hFinal->SetBinContent(i, j, 0);
    }
  }

  std::vector<TH2D*> histos(0);
  histos.push_back(h1);
  histos.push_back(h2);
  histos.push_back(hSum);
  for (Int_t i = 0; i < 3; i++) {
    cDummy->cd();
    histos[i]->SetContour(2, contourLevel);
    histos[i]->DrawCopy("CONT Z LIST");
    cDummy->Update();
    TObjArray *contours =
      (TObjArray*) gROOT->GetListOfSpecials()->FindObject("contours");
    TList *graphList = (TList*) contours->At(1);
    for (Int_t ig = 0; ig < graphList->GetEntries(); ig++) {
      TGraph *gr = (TGraph*) graphList->At(ig);
      conts.push_back((TGraph*) gr->Clone());
      if (i == 1){
        conts[i]->SetLineColor(kRed);
        conts[i]->SetTitle("Secondaries");
      }
      if (i == 2){
        conts[i]->SetLineColor(kBlue);
        conts[i]->SetTitle("Primaries+Secondaries");
      }
    }
  }
  cContours->cd();
  hFinal->DrawCopy();
  for (Int_t i = 0; i < conts.size(); i++) {
    conts[i]->Draw("SAME");
  }
}
