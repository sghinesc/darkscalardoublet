#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstddef>
#include <vector>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "../include/PythiaEvent.hh"
#include "../include/PythiaParticle.hh"

using namespace std;

void PythiaToRootSingleFile(TString inputFileName);

void PythiaToRoot(TString configFileName = ""){
  ifstream configFile(configFileName); 
  if (!configFile.is_open()){
    perror(configFileName);
    exit(1);
  }
  
  TString line;
  while(!configFile.eof()){
    configFile >> line;
    PythiaToRootSingleFile(line);
  }
}

void PythiaToRootSingleFile(TString inputFileName = ""){
  TString outputFileName(inputFileName);
  outputFileName.ReplaceAll(".txt", ".root");
  printf("%s\n", outputFileName.Data());
  
  TFile outputFile(outputFileName.Data(), "RECREATE");
  TTree *eventTree = new TTree("Particle", "Particle");
  
  //PythiaEvent *newEvt = new PythiaEvent(10000);
  PythiaParticle *particle = 0;
  eventTree->Branch("PythiaParticle", &particle);
  
  printf("================Opening file with B parameters================ \n");
  ifstream inputFile(inputFileName);
  if (!inputFile.is_open()) {
    perror(inputFileName);
    exit(1);
  }
  //Declare variables used to read the file
//  std::vector<Int_t> particleIDs{321,-321, 511, -511, 521, -521};
  std::vector<Int_t> particleIDs{511, -511, 521, -521};
  //Int_t idEventDummy = 0;
  Int_t idEvent;
  Int_t idTrack; //not used
  Int_t bMesonType;
  Double_t pxBmeson;
  Double_t pyBmeson;
  Double_t pzBmeson;
  Int_t iSource; //not used
  
  while (!inputFile.eof()) {
    //if (nEvents == 11)
    //  break;
    inputFile >> idEvent;
    inputFile >> idTrack;
    inputFile >> bMesonType;
    inputFile >> pxBmeson;
    inputFile >> pyBmeson;
    inputFile >> pzBmeson;
    inputFile >> iSource;
    
    //while (idEvent != idEventDummy){
    //  eventTree->Fill();
    //  idEventDummy++;
    //  newEvt->Clear();
    //}
    if(!(std::find(particleIDs.begin(), particleIDs.end(), bMesonType) != particleIDs.end())){
      continue;
    }
    //PythiaParticle *particle = (PythiaParticle*) newEvt->AddParticle();
    particle->SetIndex(bMesonType);
    TVector3 momentum(pxBmeson,pyBmeson,pzBmeson);
    particle->SetMomentum(momentum);
    particle->SetEventID(idEvent);
    eventTree->Fill();
  }
  
  eventTree->Print();
  outputFile.Write(0,TObject::kOverwrite);
  outputFile.Close();
}
