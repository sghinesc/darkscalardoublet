#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstddef>
#include <vector>

#include "TFile.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TH2.h"
#include "TObjArray.h"
#include "TString.h"

void BuildContoursSingleFile(TString inputFileName, std::vector<TGraph*> &conts, Int_t kFile);

Int_t nFinalBins[2];
Double_t *xFinalBins;
Double_t *yFinalBins;

void BuildContours(TString fileList = "") {
  if (fileList.CompareTo("") == 0) {
    std::cout << "Please provide a file list" << std::endl;
  }

  ifstream configFile(fileList);
  if (!configFile.is_open()) {
    perror(fileList);
    exit(1);
  }

  std::vector<TGraph*> gContours(0);
  TString line;
  Int_t kFile = 0;
  while (!configFile.eof()) {
    configFile>>line;
    BuildContoursSingleFile(line, gContours, kFile);
    kFile++;
  }

  TCanvas *cFinal = new TCanvas();
  TH2F *hFinal = new TH2F("hFinal", "Contours", nFinalBins[0], xFinalBins,
    nFinalBins[1], yFinalBins);
  cFinal->cd();
  hFinal->Draw();
  for (Int_t igraph = 0; igraph < gContours.size(); igraph++) {
    gContours[igraph]->Draw("SAME");
  }
}

void BuildContoursSingleFile(TString inputFileName, std::vector<TGraph*>& conts, Int_t kFile) {
  TFile inputFile(inputFileName, "READ");
  TArrayD *xBins = new TArrayD();
  TArrayD *yBins = new TArrayD();
  TCanvas *cDummy = new TCanvas();
  Double_t contLevels[2] = {2.3, 2.3};
  TH2D* hSignal;
  hSignal = (TH2D*) inputFile.Get("hSignalAcceptanceDSmassTheta2");

  xBins->Set(hSignal->GetXaxis()->GetNbins(), hSignal->GetXaxis()->GetXbins()->GetArray());
  yBins->Set(hSignal->GetYaxis()->GetNbins(), hSignal->GetYaxis()->GetXbins()->GetArray());
  if (kFile == 0) {
    nFinalBins[0] = xBins->GetSize();
    nFinalBins[1] = yBins->GetSize();
    xFinalBins = xBins->GetArray();
    yFinalBins = yBins->GetArray();
  }
  hSignal->DrawCopy("COLZ");

  cDummy->cd();
  hSignal->SetContour(2, contLevels);
  hSignal->DrawCopy("CONT Z LIST");
  cDummy->Update();

  TObjArray *contours =
    (TObjArray*) gROOT->GetListOfSpecials()->FindObject("contours");
  TList *graphList = (TList*) contours->At(1);
  for (Int_t ig = 0; ig < graphList->GetEntries(); ig++) {
    TGraph *gr = (TGraph*) graphList->At(ig);
    gr->SetLineWidth(2);
    if (kFile == 0)
      gr->SetLineColor(kRed);
    gr->SetTitle("");
    conts.push_back((TGraph*) gr->Clone());
  }
  inputFile.Close();
}
