#include <cstdlib>
#include <cstddef>
#include <iostream>
#include <fstream>
#include <complex>
#include <valarray>
#include <map>

#include "boost/program_options.hpp"

#include "PhysicsTools.hh"

#include "TApplication.h"

#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TGraph.h"
#include "TMultiGraph.h"

#include "TF1.h"
#include "TMath.h"
#include "TRandom3.h"

#include "TPad.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"

#include "TObjString.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TKey.h"
#include "PythiaEvent.hh"

using namespace std;
using namespace PhysicsTools;
using namespace TMath;
using namespace boost::program_options;
void UseMessage() {
  printf("To use this program do: ./executable configFilename option\n");
  printf("- option = 0 : Run in processing mode\n");
  printf("- option = 1 : Run in draw results mode\n");
}

void ParseCommandLine(int argc, char **argv, std::string &configFileName, int &option);
void ReadConfigurationFile(const std::string &configFileName);
void BookHistograms();
void ProcessInputFile(const char* filename);
void Draw();
void DrawFinal();

void ParseCommandLine(int argc, char** argv, std::string &configFileName, int &option){
  try{
    std::string inputFile;
    std::string outputFile;
    
    options_description desc("Options");
    desc.add_options()
      ("help,h", "Help screen")
      ("inputFile,i", value<std::string>(&inputFile)->multitoken()->
        zero_tokens()->composing(), "InputFile")
      ("outputFile,o", value<std::string>(&outputFile)->multitoken()->
        zero_tokens()->composing(), "OutFile")
      ("configFile,c", value<std::string>(&configFileName)->multitoken()->
        zero_tokens()->composing(), "ConfigFile")
      ("runningMode,r", value<int>(&option),"0 for normal processing; 1 for drawing mode");
      ("unreg", "Unrecognized options");
    
    command_line_parser parser{argc,argv};
    parser.options(desc).allow_unregistered().style(
      command_line_style::default_style |
      command_line_style::allow_slash_for_short);
    parsed_options parsed_options = parser.run();
    
    variables_map vm;
    store(parsed_options, vm);
    
    if (vm.count("help"))
      std::cout<<desc<<"\n";
    notify(vm);
    
    bMesonsParamsFileName = inputFile.data();
    outputFileName = outputFile.data();
  }
  catch (const error &ex){
    std::cerr << ex.what() << "\n";
  }
}
int main(int argc, char** argv) {
  //Make sure the executable is run correctly
//  if (argc != 3) {
//    UseMessage();
//    return 0;
//  }
 
  std::string configFileName;
  int option;
  ParseCommandLine(argc, argv, configFileName, option);
  //Instantiate application for permanently displaying canvases
  TApplication app("app", &argc, argv);
  
  gRandom->SetSeed(0);
  ReadConfigurationFile(configFileName);

  if (option == 0)
    printf("Running in processing mode\n");
  else if (option == 1)
    printf("Running in drawing mode\n");
  else {
    UseMessage();
    return 0;
  }

  if (option == 0) {
    //Book histograms and canvases;
    BookHistograms();

    //Loop on all entries from the input file
    ProcessInputFile(PhysicsTools::bMesonsParamsFileName);

    //Draw results and save them in output file. Note: Not final drawings
    Draw();
  } else {
    DrawFinal();
  }
  //Run TApplication to keep Canvases on
  std::cout << "Run complete!!!" << std::endl;
  app.Run(kTRUE);
  return 0;
}

void BookHistograms() {
  outputFile = new TFile(outputFileName.Data(), "RECREATE", "", 0);

  hProtonBeamSpot = new TH2D("hProtonBeamSpot", "ProtonBeamSpot", 301, -15.05, 15.05, 301, -15.05, 15.05);
  hProtonBeamSpotMatched = new TH2D("hProtonBeamSpotMatched", "ProtonBeamSpotMatched", 301, -15.05, 15.05, 301, -15.05, 15.05);

  hDarkScalarThetaX = new TH1D("hDarkScalarThetaX", "hDarkScalarThetaX", 200, 0., 1.);
  hDarkScalarThetaY = new TH1D("hDarkScalarThetaY", "hDarkScalarThetaY", 200, 0., 1.);
  hDarkScalarDecayZvtx = new TH1D("hDarkScalarDecayZvtx", "hDarkScalarDecayZvtx", (Int_t) (zFVEnd - zFVIn + 5.)*2, zFVIn - 5., zFVEnd);

  hDarkScalarToDiMuVertexPos = new
    TH2D("hDarkScalarToDiMuVertexPos", "DarkScalarToDiMuVertexPos", 2000, -20., 20., 2000., -20., 20.);
  hMuMinuxPosAtMUV3 = new TH2D("hMuMinusPosAtMUV3", "MuMinusPosAtMUV3", 200, -2., 2., 200., -2., 2.);
  hMuPlusPosAtMUV3 = new TH2D("hMuPlusPosAtMUV3", "MuPlusPosAtMUV3", 200, -2., 2., 200., -2., 2.);

  hMuMinusNStrawChambers = new TH1D("hMuMinusNStrawChambers", "MuMinusNStrawChambers", 5, -0.5, 4.5);
  hMuPlusNStrawChambers = new TH1D("hMuPlusNStrawChambers", "MuPlusNStrawChambers", 5, -0.5, 4.5);

  Double_t *binsX = new Double_t[darkScalarMassNPoints + 1];
  Double_t dx = (Log10(darkScalarMassMax) - Log10(darkScalarMassMin)) / darkScalarMassNPoints;

  Double_t *binsY = new Double_t[darkScalarThetaNPoints + 1];
  Double_t dy = (Log10(darkScalarThetaMax) - Log10(darkScalarThetaMin)) / darkScalarThetaNPoints;

  Double_t *binsY2 = new Double_t[darkScalarThetaNPoints + 1];
  Double_t dy2 = (Log10(Power(darkScalarThetaMax, 2)) - Log10(Power(darkScalarThetaMin, 2))) / darkScalarThetaNPoints;

  for (Int_t i = 0; i <= darkScalarMassNPoints; i++)
    binsX[i] = Power(10, Log10(darkScalarMassMin) + i * dx);

  for (Int_t i = 0; i <= darkScalarThetaNPoints; i++) {
    binsY[i] = Power(10, Log10(darkScalarThetaMin) + i * dy);
    binsY2[i] = Power(10, Log10(Power(darkScalarThetaMin, 2)) + i * dy2);
  }

  hSigAccDSmassTheta = new TH2D("hSignalAcceptanceDSmassTheta", Form("Event Weight (%e POT)", POT),
    darkScalarMassNPoints, binsX,
    darkScalarThetaNPoints, binsY);

  hSigAccDSmassTheta2 = new TH2D("hSignalAcceptanceDSmassTheta2", Form("Event Weight (%e POT)", POT),
    darkScalarMassNPoints, binsX,
    darkScalarThetaNPoints, binsY2);

  hRelativeAcceptance = new TH2D("hRelativeAcceptance", Form("Event Weight (%e POT)", POT),
    darkScalarMassNPoints, binsX,
    darkScalarThetaNPoints, binsY2);
  
  hBMesonMomentum = new TH1D("hBMesonMomentum", "BMesonMomentum",
    600, 0, 300);

  hDarkScalarAcceptedMom = new TH1D("hDarkScalarAcceptedMom", "Accepted Dark Scalar Momentum",
    600, 0., 300.);
  hDarkScalarRejectedMom = new TH1D("hDarkScalarRejectedMom", "Rejected Dark Scalar Momentum",
    600, 0., 300.);
}

void ReadConfigurationFile(const std::string &fileName) {
  ifstream confFile(fileName.data());
  if (!confFile.is_open()) {
    perror(fileName.data());
    exit(1);
  }

  printf("Reading configuration file: %s \n", fileName.data());

  TString Line;

  Int_t nIndexes = 0;
  TString dummy;
  Ssiz_t from = 16;
  Int_t experimentID = 0;
  while (Line.ReadLine(confFile)) {
    if (Line.BeginsWith("#")) continue;
    TObjArray *l = Line.Tokenize(" ");

//    if (Line.BeginsWith("BmesonsParamsFile= ")) {
//      PhysicsTools::bMesonsParamsFileName = ((TObjString*) (l->At(1)))->GetString();
//      printf("B mesons parameters file name = %s \n", PhysicsTools::bMesonsParamsFileName.Data());
//      continue;
//    }


    if (Line.BeginsWith("IndexesUsefulB= ")) {
      printf("Useful indexes for B mesons = ");
      while (Line.Tokenize(dummy, from, " ")) {
        nIndexes++;
        PhysicsTools::indexesUsefullB.push_back(dummy.Atoi());
        printf("%d, ", PhysicsTools::indexesUsefullB[PhysicsTools::indexesUsefullB.size() - 1]);
      }
      printf(" \n");
      continue;
    }

    if (Line.BeginsWith("FlavoursBmesons= ")) {
      printf("Flavours of B mesons considered: ");
      for (Int_t i = 0; i < nIndexes; i++) {
        PhysicsTools::flavoursBmesons.push_back(((TObjString*) (l->At(i + 1)))->GetString());
        printf("%s, ", PhysicsTools::flavoursBmesons[i].Data());
      }
      printf(" \n");
      continue;
    }

    if (Line.BeginsWith("Experiment= ")) {
      PhysicsTools::expetimentName = ((TObjString*) (l->At(1)))->GetString();
      printf("Experiment being simulated: %s \n", PhysicsTools::expetimentName.Data());
      if (!PhysicsTools::expetimentName.CompareTo("NA62"))
        experimentID = 1;
      else if (!PhysicsTools::expetimentName.CompareTo("CHARM"))
        experimentID = 2;
      else if (!PhysicsTools::expetimentName.CompareTo("SHIP"))
        experimentID = 3;
      else {
        printf("ERROR: experiment not found \n");
        exit(0);
      }
    }

    if (Line.BeginsWith("ZMUV3= ")) {
      PhysicsTools::zMUV3 = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("ZLKR= ")) {
      PhysicsTools::zLKR = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("ZFVIn= ")) {
      PhysicsTools::zFVIn = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("ZFVEnd= ")) {
      PhysicsTools::zFVEnd = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("ZTAX= ")) {
      PhysicsTools::zTAX = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("X0= ")) {
      PhysicsTools::x0 = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("Y0= ")) {
      PhysicsTools::y0 = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("Z0= ")) {
      PhysicsTools::z0 = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("Sigacceptance= ")) {
      PhysicsTools::sigAcceptance = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("POT= ")){
      PhysicsTools::POT = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("ATarget= ")) {
      PhysicsTools::aTarget = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("ZTarget= ")) {
      PhysicsTools::zTarget = ((TObjString*) (l->At(experimentID)))->GetString().Atof();
      continue;
    }

    //Dark scalar parameters
    if (Line.BeginsWith("DarkScalarMassMin= ")) {
      PhysicsTools::darkScalarMassMin = ((TObjString*) (l->At(1)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("DarkScalarMassMax= ")) {
      PhysicsTools::darkScalarMassMax = ((TObjString*) (l->At(1)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("NDarkScalarMassPoints= ")) {
      PhysicsTools::darkScalarMassNPoints = ((TObjString*) (l->At(1)))->GetString().Atoi();
      continue;
    }
    if (Line.BeginsWith("ThetaDarkScalarHiggsMin= ")) {
      PhysicsTools::darkScalarThetaMin = ((TObjString*) (l->At(1)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("ThetaDarkScalarHiggsMax= ")) {
      PhysicsTools::darkScalarThetaMax = ((TObjString*) (l->At(1)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("NThetaDarkScalarHiggsPoints= ")) {
      PhysicsTools::darkScalarThetaNPoints = ((TObjString*) (l->At(1)))->GetString().Atoi();
      continue;
    }
    if (Line.BeginsWith("HiggsDiSCouplingMin= ")) {
      PhysicsTools::darkScalarLambdaMin = ((TObjString*) (l->At(1)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("HiggsDiSCouplingMax= ")) {
      PhysicsTools::darkScalarLambdaMax = ((TObjString*) (l->At(1)))->GetString().Atof();
      continue;
    }
    if (Line.BeginsWith("NHiggsDiSCouplingPoints= ")) {
      PhysicsTools::darkScalarLambdaNPoints = ((TObjString*) (l->At(1)))->GetString().Atoi();
      continue;
    }
    if (Line.BeginsWith("Model= ")) {
      PhysicsTools::model = ((TObjString*) l->At(1))->GetString();
      continue;
    }
    if (Line.BeginsWith("DatatakingType=")) {
      Int_t dataType = ((TObjString*) (l->At(1)))->GetString().Atoi();
      if (dataType != 0 && dataType != 1) {
        std::cout << "ERROR: No valid data taking type!!" << std::endl;
        exit(3);
      }

      if (dataType == 0)
        PhysicsTools::datatakingType = "BeamDump";
      else PhysicsTools::datatakingType = "ParasiticTriggers";
    }
    if (Line.BeginsWith("ParentMeson= ")) {
      PhysicsTools::parentMesonType = ((TObjString*) (l->At(1)))->GetString();
    }
    if (model.CompareTo("Doublet") != 0)
      continue;

    if (Line.BeginsWith("DecayMode= ")) {
      PhysicsTools::bMesonDecayMode = ((TObjString*) l->At(1))->GetString();
      continue;
    }
  }

  if (model.CompareTo("Singlet") != 0 && model.CompareTo("Doublet") != 0) {
    std::cout << "ERROR: No valid model, please revise!!" << std::endl;
    exit(2);
  }
  if (datatakingType.CompareTo("BeamDump") != 0 && datatakingType.CompareTo("ParasiticTriggers") != 0) {
    std::cout << "ERROR: No valid data taking type!!" << std::endl;
    exit(3);
  }
  if (model.CompareTo("Doublet") == 0 && datatakingType.CompareTo("ParasiticTriggers") != 0) {
    std::cout << "ERROR: Associated production only available with parasitic triggers" << std::endl;
    exit(4);
  }
  printf("Done reading config file \n");
}

void ProcessInputFile(const char* filename) {
  //Class with needed functions for B-KSS decay and S->mumu decay
  //Set fixed parameters:
  PhysicsTools::MathTools tools;
  tools.SetLambda(darkScalarLambdaMax);
  //test
  //Double_t xsweight = 1.7 * 3.8E-6 / (40. * 1E5) * TMath::Power(aTarget, 1. / 3.); //sigma(pp->BB)/(sigma(pp->tot)*Nevts);
  Double_t xsweight = TMath::Power(PhysicsTools::aTarget,1./3.);
  //Double_t xsweight = 1.6E-7; //sigma(pp->BB)/(sigma(pp->tot)*Nevts);
  //vectors for scanning parameters:
  thetas.resize(darkScalarThetaNPoints);
  for (Int_t i = 0; i < darkScalarThetaNPoints; i++) {
    thetas[i] = Log10(darkScalarThetaMin) + i * (Log10(darkScalarThetaMax) - Log10(darkScalarThetaMin)) / (darkScalarThetaNPoints - 1);
    thetas[i] = Power(10, thetas[i]);
  }

  dsMasses.resize(darkScalarMassNPoints);
  for (Int_t i = 0; i < darkScalarMassNPoints; i++) {
    dsMasses[i] = Log10(darkScalarMassMin) + i * (Log10(darkScalarMassMax) - Log10(darkScalarMassMin)) / (darkScalarMassNPoints - 1);
    dsMasses[i] = Power(10, dsMasses[i]);
  }

  //Decay widths of dark scalar 
  //Loop over all masses and mixing angles and compute decay widths
  printf("Computing decay widths for different coupling parameters... \n");
  std::vector<std::vector<Double_t> > gammaStoDiMu(darkScalarMassNPoints);
  std::vector<std::vector<Double_t> > gammaStoPiPi(darkScalarMassNPoints);
  std::vector<std::vector<Double_t> > probabilityParentDecay(darkScalarMassNPoints);
  for (Int_t iMass = 0; iMass < darkScalarMassNPoints; iMass++) {
    tools.SetDarkScalarMass(dsMasses[iMass]);
    for (Int_t iTheta = 0; iTheta < darkScalarThetaNPoints; iTheta++) {
      tools.SetTheta(thetas[iTheta]);
      tools.ComputeDecayWidths();
      gammaStoDiMu[iMass].push_back(tools.GetGammaStoDiMu());
      //      if (dsMasses[iMass] < mPiPlus){
      //        gammaStoPiPi[iMass].push_back(0.0);
      //      }
      //      else{
      //        gammaStoPiPi[iMass].push_back(tools.GetGammaStoDiMu());
      //      }

      gammaStoPiPi[iMass].push_back(tools.GetGammaStoDiPi());
      //gammaStoPiPi[iMass].push_back(gammaStoDiMu[iMass][iTheta]);
      if (parentMesonType.CompareTo("K") == 0) {
        probabilityParentDecay[iMass].push_back(xsweight * 0.147 *
          Power(thetas[iTheta], 2) * 0.002 * 2 *
          tools.LambdaPhaseSpace(Power(mKPlusMeson, 2), Power(mPiPlus, 2), Power(dsMasses[iMass], 2)) /
          (2 * Power(mKPlusMeson, 2)));
        if (dsMasses[iMass] > mKPlusMeson)
          probabilityParentDecay[iMass][iTheta] = 0;
      } else {
        probabilityParentDecay[iMass].push_back(xsweight *
          5.7 * Power(1 - Power(dsMasses[iMass] / mBquark, 2), 2) *
          thetas[iTheta] * thetas[iTheta]);
      }
    }
  }

  printf("...Done!\n");

  TGraph *graphStoDiMuDW = new TGraph(darkScalarMassNPoints);
  TGraph *graphStoDiPiDW = new TGraph(darkScalarMassNPoints);
  for (Int_t iMass = 0; iMass < darkScalarMassNPoints; iMass++) {
    graphStoDiMuDW->SetPoint(iMass, dsMasses[iMass],
      gammaStoDiMu[iMass][0] / (gammaStoDiMu[iMass][0] + gammaStoPiPi[iMass][0]));
    graphStoDiPiDW->SetPoint(iMass, dsMasses[iMass],
      gammaStoPiPi[iMass][0] / (gammaStoDiMu[iMass][0] + gammaStoPiPi[iMass][0]));
  }
  TCanvas *cTest = new TCanvas();
  cTest->Divide(3, 1);
  cTest->cd(1);
  graphStoDiMuDW->Draw("AP");
  cTest->cd(2);
  graphStoDiPiDW->Draw("AP");
  cTest->Update();
  TVirtualPad *testPad = cTest->cd(3);
  testPad->SetLogx();
  testPad->SetLogy();
  //Differential decay width B->KSS function. Used for sampling invariant mass
  if (model.CompareTo("Doublet") == 0) {
    Double_t squareInvariantMassMax = 0.;
    if (bMesonDecayMode.CompareTo("Inclusive") == 0) {
      squareInvariantMassMax = TMath::Power(mBquark - mSquark, 2);
      tools.SetDaughterMass(mSquark);
      tools.SetParentMass(mBquark);
    } else if (bMesonDecayMode.CompareTo("Exclusive") == 0) {
      tools.SetParentMass(mBmeson);
    } else {
      printf("ERROR: Mode neither exclusive nor inclusive! Please revise! \n");
      exit(1);
    }


    printf("Evaluating integrals for B->SS... \n");
    tools.SetDarkScalarMass(0.);

    Int_t nPx = 500;
    Double_t normKStar = 999999.;
    Double_t normK = 999999.;


    if (bMesonDecayMode.CompareTo("Exclusive") == 0.) {
      tools.ComputeNormBtoK(normKStar, normK, nPx);
    }
    tools.InitializeBtoKSSdecayWidths(dsMasses, nPx);


    std::cout << std::endl;
    printf("...Done! \n");

    TGraph *graphBtoKstarProb = new TGraph(darkScalarMassNPoints);
    TGraph *graphBtoKProb = new TGraph(darkScalarMassNPoints);
    TGraph *graphBtoSSinclusive = new TGraph(darkScalarMassNPoints);
    TGraph *graphBtoSSinclusiveFaser = new TGraph(darkScalarMassNPoints);
    for (Int_t iMass = 0; iMass < darkScalarMassNPoints; iMass++) {
      if (bMesonDecayMode.CompareTo("Exclusive") == 0) {
        graphBtoKstarProb->SetPoint(iMass, dsMasses[iMass],
          4.3e-4 * Power(tools.GetLambda(), 2) * Power(100 / 125., 4) * gammaBtoSSExclusive[iMass][1] / normKStar);
        graphBtoKProb->SetPoint(iMass, dsMasses[iMass],
          2.8e-4 * Power(tools.GetLambda(), 2) * Power(100 / 125., 4) * gammaBtoSSExclusive[iMass][0] / normK);
      } else {
        graphBtoSSinclusive->SetPoint(iMass, dsMasses[iMass], gammaBtoSSInclusive[iMass] / gammaBPlus);
        graphBtoSSinclusiveFaser->SetPoint(iMass, dsMasses[iMass], probabilityBtoSSinclusiveFaser[iMass]);
      }
    }

    TCanvas *cTest1 = new TCanvas();
    cTest1->cd();
    graphStoDiMuDW->SetMarkerStyle(3);
    graphStoDiMuDW->SetMarkerSize(1);
    graphStoDiMuDW->Draw("ALP");
    graphStoDiMuDW->GetXaxis()->SetTitle("m_{#chi}(GeV)");
    graphStoDiMuDW->GetXaxis()->SetLimits(0.1, 10.);
    graphStoDiMuDW->GetYaxis()->SetTitle("Br(#chi->#mu^{+}#mu^{-}) = #frac{#Gamma(#chi->#mu^{+}#mu^{-})}{#Gamma(#chi->anything)}");

    TCanvas *cTest2 = new TCanvas();
    cTest2->cd();

    if (bMesonDecayMode.CompareTo("Exclusive") == 0) {
      graphBtoKstarProb->SetMarkerStyle(3);
      graphBtoKstarProb->SetMarkerSize(1);
      graphBtoKstarProb->SetLineWidth(3);
      graphBtoKstarProb->SetTitle("B->K*#chi#chi");
      graphBtoKstarProb->Draw("ALP");
      graphBtoKstarProb->GetXaxis()->SetTitle("m_{#chi}(GeV)");
      graphBtoKstarProb->GetYaxis()->SetTitle("Br(B->KSS) normalized");

      graphBtoKProb->SetMarkerStyle(4);
      graphBtoKProb->SetMarkerSize(1);
      graphBtoKProb->SetLineWidth(3);
      graphBtoKProb->SetLineColor(kRed);
      graphBtoKProb->SetTitle("B->K#chi#chi");
      graphBtoKProb->Draw("ALP");

      TMultiGraph *mg = new TMultiGraph();
      mg->Add(graphBtoKProb, "Alp");
      mg->Add(graphBtoKstarProb, "Alp");
      mg->Draw("A");
      mg->SetTitle("Exclusive");
      mg->GetXaxis()->SetTitle("m_{#chi}(GeV)");
      mg->GetYaxis()->SetTitle("Br(B->K#chi#chi)");

    } else {
      graphBtoSSinclusive->SetMarkerStyle(3);
      graphBtoSSinclusive->SetMarkerSize(1);
      graphBtoSSinclusive->SetLineColor(kBlack);
      graphBtoSSinclusive->SetLineWidth(3);
      graphBtoSSinclusive->SetTitle("m_{s} != 0");
      graphBtoSSinclusive->Draw("ALP");


      graphBtoSSinclusiveFaser->SetMarkerStyle(4);
      graphBtoSSinclusiveFaser->SetMarkerSize(1);
      graphBtoSSinclusiveFaser->SetLineColor(kRed);
      graphBtoSSinclusiveFaser->SetLineWidth(3);
      graphBtoSSinclusiveFaser->SetTitle("m_{s} = 0");
      graphBtoSSinclusive->Draw("ALP");

      TMultiGraph *mg = new TMultiGraph();
      mg->Add(graphBtoSSinclusive, "Alp");
      mg->Add(graphBtoSSinclusiveFaser, "Alp");
      mg->SetTitle("Inclusive");
      mg->Draw("A");
      mg->GetXaxis()->SetTitle("m_{#chi} (GeV)");
      mg->GetYaxis()->SetTitle("Br(b->s#chi#chi)");
    }
    cTest1->Update();
    cTest2->Update();
    cTest2->BuildLegend();
  }

  printf("================Opening file with mesons parameters==========\n");
  TFile inputFile(filename, "READ");
  if (inputFile.IsZombie()) {
    printf("Error opening file \n");
    exit(-1);
  }

  //TIter next(inputFile.GetListOfKeys());
  //TKey *key;
  TTree *inputTree;
  //PythiaEvent *pythiaEvt;

  Int_t flagIsFirstParticle = -1;
  inputTree = static_cast<TTree*> (inputFile.Get("Particle"));
  PythiaParticle *particle = new PythiaParticle();
  inputTree->SetBranchAddress("PythiaParticle", &particle);

  //  while ((key = (TKey*) next())) {
  //    if (strcmp(key->GetClassName(), "TTree")) continue;
  //    inputTree = (TTree*) inputFile.Get(key->GetName());
  //    pythiaEvt = new PythiaEvent(10000);
  //    inputTree->SetBranchAddress("PythiaEvent", &pythiaEvt);

  Int_t nEntries = inputTree->GetEntries();
  for (Int_t iEntry = 0; iEntry < nEntries; iEntry++) {
    inputTree->GetEntry(iEntry);

    if (iEntry % 1000 == 0)
      std::cout << "Porcessing event: " << iEntry << "/" << nEntries << "\r" << std::flush;

    //      TClonesArray *parentMesons = pythiaEvt->GetParticles();

    //for (UInt_t iPart = 0; iPart < pythiaEvt->GetNParticles(); iPart++) {
    //        PythiaParticle *particle = (PythiaParticle*) parentMesons->At(iPart);
    if (flagIsFirstParticle == -1) {
      flagIsFirstParticle = 0;
      Int_t isFromKaon = Abs(particle->GetIndex()) == 321 ? 1 : 0;
      Double_t parentMass = isFromKaon == 1 ? mKPlusMeson : mBquark;
      Double_t daughterMass = isFromKaon == 1 ? mPiPlus : mSquark;

      tools.SetParentMass(parentMass);
      tools.SetDaughterMass(daughterMass);
    }
    //Compute the proton beam spot
    TVector3 parentMesonStartPos;
    parentMesonStartPos.SetX(randGen.Gaus(PhysicsTools::x0, 0.00472));
    parentMesonStartPos.SetY(randGen.Gaus(PhysicsTools::y0, 0.0032));
    parentMesonStartPos.SetZ(PhysicsTools::z0);

    hProtonBeamSpot->Fill(parentMesonStartPos.X()*1000., parentMesonStartPos.Y()*1000.);
    Int_t isMatched = 0;
    //Compute thetas
    Double_t parentMesonThetaX = (0.05 * parentMesonStartPos.X()*1000. - 0.05) / 1000.;
    Double_t parentMesonThetaY = (0.031 * parentMesonStartPos.Y()*1000. - 0.01) / 1000.;
    TRotation r;
    r.RotateX(parentMesonThetaX);
    r.RotateY(parentMesonThetaY);
    //4-momentum of the B meson in laboratory frame
    TLorentzVector parentMeson4Mom;
    Double_t mParentMeson = Abs(particle->GetIndex()) == 321 ? mKPlusMeson : mBmeson;
    parentMeson4Mom.SetVectM(particle->GetMomentum(), mParentMeson);
    //parentMeson4Mom.Transform(r);
    hBMesonMomentum->Fill(parentMeson4Mom.P());
    //Loop over scanned parameters
    for (Int_t iMass = 0; iMass < darkScalarMassNPoints; iMass++) {
      if (dsMasses[iMass] > mKPlusMeson && parentMesonType.CompareTo("K") == 0)
        continue;
      tools.SetDarkScalarMass(dsMasses[iMass]);
      tools.SetBMeson4Mom(parentMeson4Mom);
      tools.ComputeDarkScalarLabMom(iMass);

      hDarkScalarThetaX->Fill(tools.GetDarkScalar4MomAccepted().X() / tools.GetDarkScalar4MomAccepted().Z());
      hDarkScalarThetaY->Fill(tools.GetDarkScalar4MomAccepted().Y() / tools.GetDarkScalar4MomAccepted().Z());
      hDarkScalarAcceptedMom->Fill(tools.GetDarkScalar4MomAccepted().P());

      TVector3 darkScalarStartPos(x0, -0.02, z0);
      
      for (Int_t iTheta = 0; iTheta < darkScalarThetaNPoints; iTheta++) {
        
        Double_t probabilityBdecay = 0.;
        if (model.CompareTo("Doublet") == 0) {
          if (bMesonDecayMode.CompareTo("Inclusive") == 0) {
            probabilityBdecay = gammaBtoSSInclusive[iMass] / gammaBPlus;
          } else {
            Int_t decayMode = tools.GetDaughterMass() == mKStarPlusMeson ? 1 : 0;
            if (decayMode == 0)
              probabilityBdecay = 4.3e-4 * Power(tools.GetLambda(), 2) * Power(100 / 125., 4) * gammaBtoSSExclusive[iMass][0] / normK;
            else
              probabilityBdecay = 2.8e-4 * Power(tools.GetLambda(), 2) * Power(100 / 125., 4) * gammaBtoSSExclusive[iMass][1] / normKStar;

          }
        } else {
          if (bMesonDecayMode.CompareTo("Inclusive"))
            probabilityBdecay = probabilityParentDecay[iMass][iTheta];
        }
        Double_t gammaSdecay = gammaStoDiMu[iMass][iTheta] + gammaStoPiPi[iMass][iTheta];
        Double_t lambdaSDecay = hc * tools.GetDarkScalar4MomAccepted().P() / (dsMasses[iMass] * gammaSdecay) * TMath::Power(10, -15);
        //Now we have the start position and momentum of the Dark scalar and we proceed to process it
        //We propagate it to the fiducial region and distribute the devay point uniformly inside it
        //        fVertex.SetParameter(0, lambdaStoDiMu);
        //        Double_t zVtx = fVertex.GetRandom();
        TVector3 vertexSdecay = tools.ComputeStoDiMuVertex(darkScalarStartPos, tools.GetDarkScalar4MomAccepted(), lambdaSDecay);
        //        TVector3 vertexStoDiMu = tools.Propagate(darkScalarStartPos, tools.GetDarkScalar4MomAccepted(), 0, zVtx);

        Double_t weightFVdecay = Exp(-(zFVIn - darkScalarStartPos.Z()) / lambdaSDecay) * (1. - Exp(-(zFVEnd - zFVIn) / lambdaSDecay));
        Double_t brDiMu = gammaStoDiMu[iMass][iTheta] / gammaSdecay;
        
        
        Double_t accDeno = 2. * probabilityBdecay * weightFVdecay;
        if (particle->GetProbability() < 0.99)
          accDeno = accDeno * particle->GetProbability();
        else
          accDeno = accDeno * (4.0E-7/40.)*xsweight*1E-5;
        
        if (datatakingType.CompareTo("BeamDump") != 0){
          accDeno = accDeno * brDiMu;
          if (particle->GetProbability() < 0.99)
            accDeno = accDeno * 0.4;
        }
        
        hRelativeAcceptance->Fill(dsMasses[iMass], Power(thetas[iTheta],2), 
          accDeno);
        //continue;
        //printf("%e %e %e %e %e %e\n", weightFVdecay, lambdaStoDiMu, gammaTotal, zFVIn, zFVEnd, darkScalarStartPos.Z());
        hDarkScalarDecayZvtx->Fill(vertexSdecay.Z());
        hDarkScalarToDiMuVertexPos->Fill(vertexSdecay.X(), vertexSdecay.Y());
        Double_t beamXcenter = -1.2E-3 * (vertexSdecay.Z() - 102.0);
        TVector2 beamCenter(beamXcenter, 0.);
        //Randomly choose between muons and pions f.s. using branching ratios
        Double_t daughterMass = PhysicsTools::mMuPlus;
        if (PhysicsTools::model.CompareTo("Singlet") == 0 && PhysicsTools::datatakingType.CompareTo("BeamDump") == 0) {
          //if (((TVector2) (vertexSdecay.XYvector() - beamCenter)).Mod() > 1.0)
          //  continue;
          Int_t isDiMu = randGen.Binomial(1, brDiMu);
          if (isDiMu != 1)
            daughterMass = PhysicsTools::mPiPlus;
        } else if (((TVector2) (vertexSdecay.XYvector() - beamCenter)).Mod() < 0.1)
          continue;

        //Now we have the starting point of the muons and we need to generate their momenta. 
        Double_t thetaStoDaughters = ACos(1. - 2. * randGen.Rndm());
        Double_t phiStoDaughters = TwoPi() * randGen.Rndm();
        std::vector<TLorentzVector> daughters4Mom(2);

        Double_t daughterMomMag = Sqrt(Power(dsMasses[iMass], 2) / 2 - Power(daughterMass, 2));
        daughters4Mom[0].SetVectM(TVector3(daughterMomMag * Sin(thetaStoDaughters) * Cos(phiStoDaughters),
          daughterMomMag * Sin(thetaStoDaughters) * Sin(phiStoDaughters),
          daughterMomMag * Cos(thetaStoDaughters)),
          daughterMass);

        daughters4Mom[1].SetVectM(-1 * TVector3(daughterMomMag * Sin(thetaStoDaughters) * Cos(phiStoDaughters),
          daughterMomMag * Sin(thetaStoDaughters) * Sin(phiStoDaughters),
          daughterMomMag * Cos(thetaStoDaughters)),
          daughterMass);

        daughters4Mom[0].Boost(tools.GetDarkScalar4MomAccepted().BoostVector());
        daughters4Mom[1].Boost(tools.GetDarkScalar4MomAccepted().BoostVector());

        if (datatakingType.CompareTo("BeamDump") == 0) {
          if (daughters4Mom[0].P() < 5. || daughters4Mom[0].P() > 400. ||
            daughters4Mom[1].P() < 5. || daughters4Mom[1].P() > 400.)
            continue;
        } else if (daughters4Mom[0].P() < 5 || daughters4Mom[0].P() > 90. ||
          daughters4Mom[1].P() < 5. || daughters4Mom[1].P() > 90.)
          continue;
        //Momentum and starting point for the muons are now known. We can propagate them
        //along the beamline
        Double_t charge[2] = {1., 1.};
        if (randGen.Binomial(1, 0.5))
          charge[0] = -1;
        else
          charge[1] = -1;


        Int_t nChambers1 = tools.IsInStraw(vertexSdecay, daughters4Mom[0], charge[0]);
        Int_t nChambers2 = tools.IsInStraw(vertexSdecay, daughters4Mom[1], charge[1]);

        if (charge[0] < 0) {
          hMuMinusNStrawChambers->Fill(nChambers1);
          hMuPlusNStrawChambers->Fill(nChambers2);
        } else {
          hMuMinusNStrawChambers->Fill(nChambers2);
          hMuPlusNStrawChambers->Fill(nChambers1);
        }

        if (nChambers1 != 4 || nChambers2 != 4)
          continue;
        //TVector3 daugtherPosAtMag[2] = {tools.Propagate(vertexSdecay, daughters4Mom[0], charge[0], zMagnet),
        //  tools.Propagate(vertexSdecay, daughters4Mom[1], charge[1], zMagnet)};

        beamXcenter = -1.2E-3 * (zMagnet - 102.0);
        beamCenter.Set(beamXcenter, 0.);
        Double_t distToCenter[2];
        Int_t flagBreak = 0;
        //        for (Int_t iDaughter = 0; iDaughter < 2; iDaughter++) {
        //          distToCenter[iDaughter] = (daugtherPosAtMag[iDaughter].XYvector() - beamCenter).Mod();
        //          if (distToCenter[iDaughter] < 0.1 /*|| distToCenter[iDaughter] > 1.0*/) {
        //            flagBreak = 1;
        //            break;
        //          }
        //        }
        //        if (flagBreak != 0)
        //         continue;

        TVector3 daughtersPos[2] = {tools.Propagate(vertexSdecay, daughters4Mom[0], charge[0], zMUV3),
          tools.Propagate(vertexSdecay, daughters4Mom[1], charge[1], zMUV3)};

        if (charge[0] < 0) {
          hMuMinuxPosAtMUV3->Fill(daughtersPos[0].X(), daughtersPos[0].Y());
          hMuPlusPosAtMUV3->Fill(daughtersPos[1].X(), daughtersPos[1].Y());
        } else {
          hMuMinuxPosAtMUV3->Fill(daughtersPos[1].X(), daughtersPos[1].Y());
          hMuPlusPosAtMUV3->Fill(daughtersPos[0].X(), daughtersPos[0].Y());
        }

        beamXcenter = -1.2E-3 * (zMUV3 - 102.0);
        beamCenter.SetX(beamXcenter);
        if (TMath::Abs(daughtersPos[0].X() - beamXcenter) > 1.32 || TMath::Abs(daughtersPos[0].Y()) > 1.32 ||
          TMath::Abs(daughtersPos[1].X() - beamXcenter) > 1.32 || TMath::Abs(daughtersPos[1].Y()) > 1.32)
          continue;

        if (((TVector2) (daughtersPos[0].XYvector() - beamCenter)).Mod() < 0.1 ||
          ((TVector2) (daughtersPos[1].XYvector() - beamCenter)).Mod() < 0.1)
          continue;

        Double_t eventWeight = 2. * probabilityBdecay * weightFVdecay;
        if (particle->GetProbability() < 0.99) {
          eventWeight = eventWeight * particle->GetProbability();
        } else {
          eventWeight = eventWeight * (4.0E-7/40.)* 1E-5*xsweight;
        }
        if ((model.CompareTo("Singlet") == 0 && datatakingType.CompareTo("BeamDump") != 0) ||
          model.CompareTo("Doublet") == 0){
          eventWeight = eventWeight * brDiMu;
          if (particle->GetProbability() < 0.99)
            eventWeight = eventWeight * 0.4;
        }
        else eventWeight = eventWeight;

        if (model.CompareTo("Singlet") == 0)
          eventWeight = eventWeight;
        Double_t fillVal = eventWeight*POT;
        hSigAccDSmassTheta->Fill(dsMasses[iMass], thetas[iTheta],fillVal);
        hSigAccDSmassTheta2->Fill(dsMasses[iMass], TMath::Power(thetas[iTheta], 2), fillVal);
        
        isMatched = 1;
      }
    }

    if (isMatched > 0)
      hProtonBeamSpotMatched->Fill(parentMesonStartPos.X()*1000., parentMesonStartPos.Y()*1000.);
    //}

    //      std::cout << "Porcessing event: " << iEntry << "\r" << std::endl;
    //      std::cout << "####################### Analysis completed ##################" << std::endl;
    //      std::cout << std::endl;

  }

}

void Draw() {
  std::cout << "#########Start Drawing and saving results###########" << std::endl;
  hBMesonMomentum->DrawCopy();
  hProtonBeamSpot->DrawCopy();
  //Create output file
  //  cDarkScalarDecay = new TCanvas();
  //  cDarkScalarDecay->Divide(2, 1);
  //  cDarkScalarDecay->cd(1);
  hDarkScalarDecayZvtx->DrawCopy("HIST");
  //  cDarkScalarDecay->cd(2);
  hDarkScalarToDiMuVertexPos->DrawCopy("COLZ");

  //  cMuonPosAtMuv3 = new TCanvas();
  //  cMuonPosAtMuv3->Divide(2, 1);
  //  cMuonPosAtMuv3->cd(1);
  hMuMinuxPosAtMUV3->DrawCopy("COLZ");
  //  cMuonPosAtMuv3->cd(2);
  hMuPlusPosAtMUV3->DrawCopy("COLZ");

  //  cMuonNStrawChambers = new TCanvas();
  //  cMuonNStrawChambers->Divide(2,1);
  //  cMuonNStrawChambers->cd(1);
  hMuMinusNStrawChambers->DrawCopy();
  //  cMuonNStrawChambers->cd(2);
  hMuPlusNStrawChambers->DrawCopy();

  //  cDarkScalarThetaXY = new TCanvas();
  //  cDarkScalarThetaXY->Divide(2, 1);
  //  cDarkScalarThetaXY->cd(1);
  hDarkScalarThetaX->DrawCopy();
  //  cDarkScalarThetaXY->cd(2);
  hDarkScalarThetaY->DrawCopy();

  hDarkScalarAcceptedMom->DrawCopy();
  hDarkScalarRejectedMom->DrawCopy();
  //  cSignalAcceptance = new TCanvas();
  //  cSignalAcceptanceDSmassTheta2 = new TCanvas();
  //bMesonDecayChannel.AddText(Form("B->%s", bMesonDecayProducts.Data()));
  //  cSignalAcceptance->cd();
  hSigAccDSmassTheta->GetXaxis()->SetTitle("m_{#chi} (GeV)");
  hSigAccDSmassTheta->GetYaxis()->SetTitle("#theta");
  hSigAccDSmassTheta->DrawCopy("COLZ");

  //  cSignalAcceptanceDSmassTheta2->cd();
  hSigAccDSmassTheta2->GetXaxis()->SetTitle("m_{#chi} (GeV)");
  hSigAccDSmassTheta2->GetYaxis()->SetTitle("#theta^{2}");
  hSigAccDSmassTheta2->DrawCopy("COLZ");

  outputFile->Write();
  outputFile->Close();
  delete outputFile;
  outputFile = NULL;

  std::cout << "...Done!" << std::endl;
}

void DrawFinal() {



  TH2F *hFinal;
  TCanvas *cInitial = new TCanvas();
  TCanvas *cFinal = new TCanvas();
  TCanvas *cDummy = new TCanvas();
  TArrayD *xBins = new TArrayD();
  TArrayD *yBins = new TArrayD();
  Double_t contLevels[2] = {2.3, 2.3};
  std::vector<TGraph*> graphConts(0);

  TString fileNameList[2] = {"out_ToyMC_Singlet_BeamDump_Secondaries_test.root",
    "out_ToyMC_Singlet_BeamDump_new.root"};
  for (Int_t iFile = 0; iFile < 2; iFile++) {


    for (Int_t i = 0; i < 2; i++) {
      //open file containing results
      // TFile inputFile(Form("out_ToyMC_%s_new.root", decayMode[i].Data()), "READ", "");
      TFile inputFile(fileNameList[iFile].Data(), "READ", "");

      TVirtualPad *dummyPad = cInitial->cd();
      hSigAccDSmassTheta2 = (TH2D*) inputFile.Get("hSignalAcceptanceDSmassTheta2");
      if (i == 0) {
        xBins->Set(hSigAccDSmassTheta2->GetXaxis()->GetNbins(), hSigAccDSmassTheta2->GetXaxis()->GetXbins()->GetArray());
        yBins->Set(hSigAccDSmassTheta2->GetYaxis()->GetNbins(), hSigAccDSmassTheta2->GetYaxis()->GetXbins()->GetArray());
      }

      hSigAccDSmassTheta2->SetTitle(Form("Event Weight (%e POT)", POT));
      hSigAccDSmassTheta2->DrawCopy("COLZ");
      dummyPad->SetLogz();

      cDummy->cd();
      hSigAccDSmassTheta2->SetContour(2, contLevels);
      hSigAccDSmassTheta2->DrawCopy("CONT Z LIST");
      cDummy->Update();


      TObjArray *contours =
        (TObjArray*) gROOT->GetListOfSpecials()->FindObject("contours");
      TList *graphList = (TList*) contours->At(1);
      for (Int_t ig = 0; ig < graphList->GetEntries(); ig++) {
        TGraph *gr = (TGraph*) graphList->At(ig);
        if (iFile == 0)
          gr->SetLineColor(kRed);
        gr->SetLineWidth(2);
        if (ig == 0)
          gr->SetTitle("");
        else
          gr->SetTitle("");
        graphConts.push_back((TGraph*) gr->Clone());
      }
    }
  }
  //  Int_t nx = 10 * darkScalarMassNPoints;
  //  Int_t ny = 10 * darkScalarThetaNPoints;
  //  Double_t *binsX = new Double_t[nx + 1];
  //  Double_t dx = (Log10(darkScalarMassMax) - Log10(darkScalarMassMin)) / (nx);
  Int_t nx = 100 * darkScalarMassNPoints;
  Int_t ny = 10 * darkScalarThetaNPoints;
  Double_t *binsX = new Double_t[nx + 1];
  Double_t dx = (Log10(10) - Log10(0.1)) / (nx);

  Double_t *binsY = new Double_t[ny + 1];
  Double_t dy = (Log10(darkScalarThetaMax) - Log10(darkScalarThetaMin)) / (ny);

  Double_t *binsY2 = new Double_t[ny + 1];
  Double_t dy2 = (Log10(Power(darkScalarThetaMax, 2)) - Log10(Power(darkScalarThetaMin, 2))) / (ny);

  for (Int_t i = 0; i <= nx; i++)
    binsX[i] = Power(10, Log10(0.1) + i * dx);

  for (Int_t i = 0; i <= ny; i++) {
    binsY[i] = Power(10, Log10(darkScalarThetaMin) + i * dy);
    binsY2[i] = Power(10, Log10(Power(darkScalarThetaMin, 2)) + i * dy2);
  }

  cFinal->cd();
  hFinal = new TH2F("hFinal", "Contours at level 2.3",
    nx, binsX, ny, binsY2);
  hFinal->Draw();
  for (UInt_t i = 0; i < graphConts.size(); i++) {
    //graphConts[i]->Print();
    graphConts[i]->Draw("SAME");
  }
  //hFinal->GetXaxis()->SetRangeUser(0.2, 0.3);
  hFinal->GetXaxis()->SetTitle("m_{#chi} (GeV)");
  hFinal->GetYaxis()->SetTitle("#theta^{2}");
  //hFinal->GetYaxis()->SetRangeUser(1e-4,2e-2);
  //cFinal->GetPad(1)->SetLogx();
  //cFinal->GetPad(1)->SetLogy();
  cFinal->BuildLegend();
}