# Dark Scalar doublet Toy MC

This is an application written in C++ with ROOT libraries for simulating the 
decay of a B meson to a K meson and a pair of dark scalars.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

There are a few things you need in order to run this piece of software:

* [CMake (version > 3.6)](https://cmake.org/) - usually from distribution repos
* [ROOT](https://root.cern.ch/) - Dependency Management
* a C++ compiler

### Installing

A step by step series of examples that tell you how to get a development env running

* Obtain the code from gitlab

```bash
git clone ssh://git@gitlab.cern.ch:7999/sghinesc/darkscalardoublet.git
```

* Eneter the directory containing the repository and run CMake after sourcing ROOT

```bash
cd darkscalardoublet
source path/to/ROOT/build/bin/thisroot.sh
cmake .
```

* Run make

```bash
make
```

* Run the executable (the executable name is written in the last two lines of the CMakeLists.txt)

```bash
./executable configFile
```

the default config file is called ToyMC.conf