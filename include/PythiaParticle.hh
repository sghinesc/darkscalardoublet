#ifndef PYTHIAPARTICLE_HH
#define PYTHIAPARTICLE_HH

#include "TObject.h"
#include "TVector3.h"

class PythiaParticle : public TObject{
public:
    PythiaParticle();
    virtual ~PythiaParticle();
    
    void SetMomentum(const TVector3 &p);
    void SetIndex(Int_t id){m_index = id;}
    void SetProbability(Double_t prob) {m_probability = prob;} 
    void SetEventID(Int_t id) {m_eventID = id;}
    
    TVector3 GetMomentum() const {return m_momentum;}
    Int_t GetIndex()const {return m_index;}
    Double_t GetProbability() const {return m_probability;}
    Int_t GetEventID() const {return m_eventID;}
    
private:

    Int_t m_index;
    TVector3 m_momentum;
    Double_t m_probability;
    Int_t m_eventID;
    ClassDef(PythiaParticle, 1);
};

#endif /* PYTHIAPARTICLE_HH */

