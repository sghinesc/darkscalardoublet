#ifndef PHYSICSTOOLS_HH
#define PHYSICSTOOLS_HH

#include <vector>

#include "Rtypes.h"
#include "TString.h"
#include "TVector3.h"
#include "TLorentzVector.h"

#include "TH2.h"
#include "TH1.h"
#include "TCanvas.h"

#include "TRandom3.h"
#include "TF1.h"

#include "TPaveText.h"

namespace PhysicsTools {

    class MathTools {
    public:

        MathTools();

        virtual ~MathTools() {
        };
        Double_t FaserDecayWidthBSSInclusive(Double_t x);
        void ComputeNormBtoK(Double_t &normKstar, Double_t &normK, const Int_t &nPx);
        Double_t DiffDecayWidthBSSInclusive(Double_t *x, Double_t *pars);
        Double_t LambdaPhaseSpace(const Double_t a, const Double_t b, const Double_t c);

        Double_t DiffDecayWidthBKSS(Double_t *x, Double_t *pars);
        Double_t PhaseSpaceBKSS(Double_t invariantMassSS, Double_t dsMass);

        Double_t DiffDecayWidthBKstarSS(Double_t *x, Double_t *pars);
        Double_t PhaseSpaceBKstarSS(Double_t invariantMassSS, Double_t dsMass);

        Double_t BkFormFactor2(Double_t invariantMassSS);
        Double_t MagicFunc(Double_t x);
        TVector3 ComputeStoDiMuVertex(const TVector3 &sStartPos, const TLorentzVector &sStart4Mom, const Double_t lambda);
        std::vector<TLorentzVector> Compute2DaughterDecay(const TLorentzVector &parent4Mom);
        TVector3 Propagate(const TVector3 &position, TLorentzVector &momentum, const Double_t charge, const Double_t zEnd);
        Int_t IsInStraw(const TVector3 &startPos, TLorentzVector &momentum, const Double_t charge);
        void ComputeDecayWidths();
        Double_t StoPiPiFromFactors(const Double_t &s);
        
        void InitializeBtoKSSdecayWidths(const std::vector<Double_t> &dsMasses, const Int_t &nPx);
        
        void ComputeDarkScalarLabMom(const Int_t iMass);
        void DecayBtoDoublet(const Int_t iMass);
        void DecayBtoSinglet(const Int_t iMass);
        //Getters
         
        Double_t GetLambda() {
            return m_lambda;
        }

        Double_t GetTheta() {
            return m_theta;
        }

        Double_t GetDarkScalarMass() {
            return m_darkScalarMass;
        }

        Double_t GetGammaStoDiMu() {
            return m_gammaStoDiMu;
        }

        Double_t GetGammaStoDiPi() {
            return m_gammaStoPiPi;
        }

        Double_t GetSquareInvariantMassSS() {
            return m_squareInvariantMassSS;
        }

        Double_t GetParentMass() {
            return m_parentMass;
        }

        Double_t GetDaughterMass() {
            return m_daughterMass;
        }
        
        TLorentzVector GetBMeson4Mom(){
            return m_bMeson4Mom;
        }
        
        TLorentzVector GetDarkScalar4MomAccepted(){
            return m_DarkScalar4MomAccepted;
        }
        
        TLorentzVector GetDarkScalar4MomRejected(){
            return m_DarkScalar4MomRejected;
        }
        //Setters

        void SetLambda(Double_t val) {
            m_lambda = val;
        }

        void SetTheta(Double_t val) {
            m_theta = val;
        }

        void SetDarkScalarMass(Double_t val) {
            m_darkScalarMass = val;
        }

        void SetSquareInvariantMassSS(Double_t val) {
            m_squareInvariantMassSS = val;
        }

        void SetParentMass(Double_t val) {
            m_parentMass = val;
        }

        void SetDaughterMass(Double_t val) {
            m_daughterMass = val;
        }
        
        void SetBMeson4Mom(TLorentzVector &vect){
            m_bMeson4Mom = vect;
        }
        
        void SetDarkScalar4MomAccepted(TLorentzVector &vect){
            m_DarkScalar4MomAccepted = vect;
        }
        
        void SetDarkScalar4MomRejected(TLorentzVector &vect){
            m_DarkScalar4MomRejected = vect;
        }
        //Gammas are not set, they are computed and retrieved
    private:
        Double_t m_lambda; //quartic coupling
        Double_t m_theta; //mixing angle
        Double_t m_darkScalarMass;
        Double_t m_gammaStoDiMu;
        Double_t m_gammaStoPiPi;
        Double_t m_squareInvariantMassSS;
        Double_t m_parentMass;
        Double_t m_daughterMass;
        TLorentzVector m_bMeson4Mom;
        TLorentzVector m_DarkScalar4MomAccepted;
        TLorentzVector m_DarkScalar4MomRejected;
    };

    //Physical constants
    extern const Double_t hc; //GeV*fm
    extern const Double_t hbar; //GeV*s
    extern const Double_t alpha;
    extern const Double_t pi; //3.1415
    extern const Double_t wilsonCoeffDM2;
    extern const Double_t ec; //Electric charge conversion
    extern const Double_t mg; //Magnetic induction conversion
    extern const Double_t zMagnet;
    extern const Double_t dMagnet;
    extern const Double_t bMagnet;
    extern Double_t wilsonCoeffL;
    extern Double_t wilsonCoeffR;
    //Masses
    extern const Double_t mBquark;
    extern const Double_t mTquark;
    extern const Double_t mSquark;
    extern const Double_t mBmeson;
    extern const Double_t mKPlusMeson;
    extern const Double_t mK0Meson;
    extern const Double_t mKStarPlusMeson;
    extern const Double_t mKStar0Meson;
    extern Double_t mKmeson;
    extern const Double_t mWboson;
    extern const Double_t mMuPlus;
    extern const Double_t mPiPlus;
    extern const Double_t mPi0;
    extern const Double_t mVhiggs;
    extern const Double_t xt2; //fraction M_top/m_W squared

    //Quantities based on masses
    void ComputeAditionalConstants();
    extern Double_t mBmKsum; // mBmeson^2 + mKmeson^2
    extern Double_t mBmKdiff2; // (mBmeson^2 - mKmeson^2)^2
    extern Double_t mBmesonThree; // mBmeson^3
    extern Double_t mBmesonMtwo; // mBmeson^-2
    extern Double_t mBmesonMfour; // mBmeson^-4
    extern Double_t mBmesonMsix; // mBmeson^-6
    extern Double_t mBquarkTwo; //mBquark^2

    //Decay widths
    extern const Double_t gammaBPlus;
    extern const Double_t gammaBZero;
    //Quantities and parameters for running the ToyMC
    extern TString parentMesonType;
    extern TString bMesonsParamsFileName;
    extern TString outputFileName;
    extern std::vector<Int_t> indexesUsefullB;
    extern std::vector<TString> flavoursBmesons;
    
    //Experiment parameters:
    extern TString expetimentName;
    extern TString datatakingType;
    extern Double_t zMUV3;
    extern Double_t zLKR;
    extern Double_t zFVIn;
    extern Double_t zFVEnd;
    extern Double_t zTAX;
    extern Double_t x0;
    extern Double_t y0;
    extern Double_t z0;
    extern Double_t sigAcceptance;
    extern Double_t POT;
    extern Double_t aTarget;
    extern Double_t zTarget;
    extern const Double_t beamHoleRadius;
    extern const Double_t zStraw[4];
    extern const Double_t xStrawCenter[4];
    extern const Double_t rStrawChambers;
    //Dark scalar parameters to scan
    extern Double_t darkScalarMassMin;
    extern Double_t darkScalarMassMax;
    extern Int_t darkScalarMassNPoints;
    extern Double_t darkScalarThetaMin;
    extern Double_t darkScalarThetaMax;
    extern Int_t darkScalarThetaNPoints;
    extern Double_t darkScalarLambdaMin;
    extern Double_t darkScalarLambdaMax;
    extern Int_t darkScalarLambdaNPoints;
    extern TString bMesonDecayMode;
    extern TString model;
    //Histograms and output file
    //Dark Scalar production
    extern TFile *outputFile;
    extern TCanvas *cDarkScalarThetaXY;
    extern TH1D *hDarkScalarThetaX;
    extern TH1D *hDarkScalarThetaY;
    
    //proton beam
    extern TCanvas *cProtonBeam;
    extern TH2D *hProtonBeamSpot;
    extern TH2D *hProtonBeamSpotMatched;
    //DarkScalar Decay
    extern TCanvas *cDarkScalarDecay;
    extern TH1D *hDarkScalarDecayZvtx;
    extern TH2D *hDarkScalarToDiMuVertexPos;

    extern TH1D *hDarkScalarAcceptedMom;
    extern TH1D *hDarkScalarRejectedMom;
    //Muon propagation and detection
    extern TCanvas *cMuonPosAtMuv3;
    extern TH2D *hMuPlusPosAtMUV3;
    extern TH2D *hMuMinuxPosAtMUV3;
    
    extern TCanvas *cMuonNStrawChambers;
    extern TH1D *hMuMinusNStrawChambers;
    extern TH1D *hMuPlusNStrawChambers;
    //Signal acceptance
    extern TCanvas *cSignalAcceptance;
    extern TCanvas *cSignalAcceptanceDSmassTheta2;
    extern TCanvas *cBrBtoXmissE;
    extern TH2D *hSigAccDSmassTheta;
    extern TH2D *hSigAccDSmassTheta2;
    extern TH2D *hRelativeAcceptance;
    
    extern TCanvas *cBMeson;
    extern TH1D *hBMesonMomentum;
    //Random number generator
    extern TRandom3 randGen;

    extern std::vector<TF1*> funcDecayWidthInclusive;
    extern std::vector<Double_t> gammaBtoSSInclusive;
    extern std::vector< std::vector<TF1*> > funcDecayWidthExclusive; //B->K and B->K*
    extern std::vector< std::vector<Double_t> > gammaBtoSSExclusive; // Same as above
    extern std::vector<Double_t> probabilityBtoKstar;
    extern std::vector<Double_t> probabilityBtoSSinclusiveFaser;
    extern std::vector<Double_t> dsMasses;
    extern std::vector<Double_t> thetas;
    extern Double_t normK;
    extern Double_t normKStar;
}
#endif /* PHYSICSTOOLS_HH */

