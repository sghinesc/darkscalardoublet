#ifndef PYTHIAEVENT_HH
#define PYTHIAEVENT_HH

#include "TObject.h"

#include "TClonesArray.h"

#include "PythiaParticle.hh"

class PythiaEvent : public TObject {
public:
    PythiaEvent();
    PythiaEvent(Int_t nMaxParticles);
    virtual ~PythiaEvent();
    
    PythiaParticle * AddParticle();
    void Clear(Option_t* = "");
    
    Int_t GetNParticles() {return m_NParticles;} 
    TClonesArray* GetParticles() {return m_particles;} 
private:

    Int_t m_NParticles;
    TClonesArray* m_particles;
    
    ClassDef(PythiaEvent, 1);
};

#endif /* PYTHIAEVENT_HH */

